#TypeScript framework for creating web games and interactive multimedia software.#

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Folder & file structure:

```
#!html

IvaneMain.ts - The main and only file you have to include in your TypeScript project.
jslib/       - External JavaScript library dependencies you have to include in your HTML files.
definitions/ - Definitions for External dependencies. 
src/         - Where the TypeScript source files reside.
tests/       - Various test cases. Few regression tests.

```

#Use example#

**[New project template](https://bitbucket.org/ivaneg/ivanets_newproject)**

```Javascript
/// main.ts
/// <reference path="ivanets/IvaneMain.ts" />

class GClass extends Ivane.GameClasses.GameClass
{
    scene:THREE.Scene
    camera:THREE.PerspectiveCamera
    renderer:THREE.WebGLRenderer
    cube:THREE.Mesh

    aspectRatio:number

    constructor()
    {
        super()

        //Adding canvas element as a child of document.body for rendering content
        this.initCanvas2D(window.innerWidth, window.innerHeight, document.getElementById("canvasContainer"))

        this.aspectRatio = this.canvasElement.width / this.canvasElement.height

        this.scene = new THREE.Scene()

        this.camera = new THREE.PerspectiveCamera(45, this.aspectRatio, 0.1, 1000)
        this.camera.position.z = 10
        this.camera.position.y = 10
        this.camera.lookAt(new THREE.Vector3(0.0, 0.0, 0.0))

        this.renderer = new THREE.WebGLRenderer({
            canvas: this.canvasElement
        })

        this.renderer.setClearColor(new THREE.Color(0xFFeeee), 1.0)

        this.renderer.setSize(this.canvasElement.width, this.canvasElement.height)

        this.cube = new THREE.Mesh(new THREE.BoxGeometry(1.0, 1.0, 1.0), new THREE.MeshBasicMaterial(
            {
                color: 0xFF6666
            }
        ))                

        this.scene.add(this.cube)
        
        window.onresize = (e)=>{
            this.handleOnWindowResize()
        }
    }

    gameStep()
    {
        if(this.inputsManager.keyIsDown(Ivane.Inputs.KeyCodes.right_arrow))
        {
            this.cube.rotation.y += 1.0 * this.deltaTime
        }

        if(this.inputsManager.keyIsDown(Ivane.Inputs.KeyCodes.left_arrow))
        {
            this.cube.rotation.y -= 1.0 * this.deltaTime
        }        

        this.renderer.clear(true, true)
        this.renderer.render(this.scene, this.camera)
    }
    
    handleOnWindowResize()
    {
        this.canvasElement.width = window.innerWidth
        this.canvasElement.height = window.innerHeight
        
        this.aspectRatio = this.canvasElement.width / this.canvasElement.height
        
        this.camera.fov = 45;
        this.camera.aspect = this.aspectRatio
        this.camera.updateProjectionMatrix()
        
        this.renderer.setSize(this.canvasElement.width, this.canvasElement.height)
    }
}

var gClass:GClass

window.onload = (e) =>
{
    gClass = new GClass()
    gClass.run()

}
```

```
tsc --target es5 main.ts --outFile main.js
```

```html
<!-- index.html -->

<head>
    <style>
        
        #canvasContainer {
            width: 100%;
            height: 100%;
        }

        canvas {
            padding: 0;
            margin: auto;
            display: block;
        /* width: 1280px;
            height: 720px; */
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
        }

        body {
            overflow: hidden;
            margin: 0px;
            padding: 0px;
        }

        body,
        html {
            position: fixed;
            -webkit-user-select: none;
            -webkit-tap-highlight-color: rgba(0,0,0,0); 
        } 
        
    </style>
</head>

<script src="ivanets/jslib/threejs/three.js"></script>
<script src="ivanets/jslib/gl-matrix/gl-matrix.js"></script> 
<script src="main.js"></script> 

<div id="canvasContainer">
    
</div>
```