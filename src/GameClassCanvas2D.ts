/*
 * Author Ivane Gegia http://ivane.info
 */

///<reference path="../definitions/threejs/three.d.ts" />
///<reference path="DeltaTime.ts"/>
///<reference path="CanvasInputsManager.ts"/>
///<reference path="ThreeJSHelpers/ThreeJSHelpers.ts"/>

module Ivane.Canvas2DHelpers
{
	/**
	 * წერტილის აღმწერი კლასი
	 */
	export class Point2D
	{
		x:number
		y:number
		
		constructor()
		{
			this.x = 0.0
			this.y = 0.0
		}
		
		initWithPoint2D(point2D:Point2D)
		{
			this.x = point2D.x
			this.y = point2D.y
		}
		
		private deltaX:number = 0.0
		private deltaY:number = 0.0
		
		distance(tillPoint:Point2D):number
		{
			this.deltaX = tillPoint.x - this.x
			this.deltaY = tillPoint.y - this.y
			
			return Math.sqrt( this.deltaX * this.deltaX + this.deltaY * this.deltaY )
		}
	}
	
	/**
	 * კლასი აღწერს მონაკვეთს რომელიც გავლებულია a,b წერტილებზე
	 */
	export class Line2D
	{
		a:Point2D
		b:Point2D
		
		constructor()
		{
			this.a = new Point2D()
			this.b = new Point2D()
		}
	}
	
	export class GameClassCanvas2D
	{
		canvasElement:HTMLCanvasElement
		canvas2DContext:CanvasRenderingContext2D
		
		deltaTime:number
		inputsManager:Ivane.Inputs.CanvasInputsManager	

		private deltaTimeComputer:Ivane.Time.DeltaTimeComputer
		private enableMiddleMouseCameraDrag:boolean = false
		private initCalled:boolean = false
		
		private canvasCenterX:number
		private canvasCenterY:number
		
		worldOffset:Point2D = new Point2D()
										
		initCanvas2D(width:number, height:number, canvasContainer:HTMLElement):void
		{
			var viewRatio = width / height
			
			this.canvasElement = <HTMLCanvasElement>document.createElement("canvas")
			this.canvasElement.width = width
			this.canvasElement.height = height	

						
			canvasContainer.appendChild(this.canvasElement)
			
			this.canvas2DContext = <CanvasRenderingContext2D>this.canvasElement.getContext("2d");
			
			this.deltaTimeComputer = new Ivane.Time.DeltaTimeComputer()
			this.deltaTime = this.deltaTimeComputer.getDeltaTimeInSeconds()
			
			this.inputsManager = new Ivane.Inputs.CanvasInputsManager()
			this.inputsManager.startProcessingInputFor(this.canvasElement)
			
			this.initCalled = true
			
			this.canvasCenterX = this.canvasElement.width / 2
			this.canvasCenterY = this.canvasElement.height / 2
		}
				
		run():void
		{
			if(this.initCalled == false)
			{
				console.error("GameClassThreeJS.initWithOrthoCamera was not called!")	
				return
			}
			
			this.animationFrameFunction()
		}	

		/**
		 * გადაყავს სამყაროს კოორდინატები ხედის პიქსელ კოორდინატებში, აბრუნებს Y ღერძს.
		 * ითვალისწინებს worldOffset-ს
		 * 
		 * @param worldX სამყაროს X კოორდინატი
		 * @param worldY სამყაროს Y კოორდინატი
		 */
		convertPointFromWorldToPixelCoordinates(worldX: number, worldY: number, scale: number, pixelCoordinate_out: Point2D, withWorldOffset?: boolean) {
			
			if (withWorldOffset === true) {				
				pixelCoordinate_out.x = (worldX + this.worldOffset.x) * scale
				pixelCoordinate_out.y = (worldY + this.worldOffset.y) * scale * -1.0
			}
			else
			{
				pixelCoordinate_out.x = worldX * scale
				pixelCoordinate_out.y = worldY * scale * -1.0				
			}
			
		}
				
		/**
		 * გადაყავს ხედის პიქსელ კოოდინატი სამყაროს კოორდიანტში, აბრუნებს Y ღერძს.
		 * ითვალისწინებს worldOffset-ს
		 * 
		 * @param viewPixelX პიქსელის კოოდრინატი Canvas2D-ში
		 * @param viewPixelY პიქსელის კოორდინატი Canvas2D-ში
		 * @param withWorldOffset გაითვალისწინოს თუ არა სამყაროს წანაცვლება ისე რომ ხედის კოორდინატი სწორ სამყაროს წერტილში გადაიყვანოს.
		 */
		convertViewPixelCoordinatesToWorldCoordiantes(viewPixelX: number, viewPixelY: number, pixelsPerWorldUnit: number, worldCoordinates_out: Point2D, withWorldOffset?: boolean) {
			//მასშტაბის გადათვლა
			worldCoordinates_out.x = viewPixelX * pixelsPerWorldUnit
			worldCoordinates_out.y = viewPixelY * pixelsPerWorldUnit * -1


			//როცა ხედის პიქსელური კოოდინატებიდან გადმოვგვყავს წანაცვლებული სამყაროს კოორდინატებში, წერტილი სამყაროს წანაცვლების საპირისპიროდ უნდა წანაცვლდეს რომ
			//კორექტული სამყაროსეული კოორდინატები გამოვიდეს. 
			if (withWorldOffset === true) {
				worldCoordinates_out.x += ( this.worldOffset.x * -1)
				worldCoordinates_out.y += (this.worldOffset.y * -1)
			}
		}
		
		private convertCenterPixelCoordinatesToWorldCoordinatesWithRespectToWorldOffset_Point2D:Ivane.Canvas2DHelpers.Point2D = new Ivane.Canvas2DHelpers.Point2D()
		
		/**
		 * გადაყავს პიქსელური კოორდინატები(პიქსელური ხედის ცენტრიდან ათვლილი), სამყაროს კოოდინატებში(სამყაროს წანაცვლების/"კამერის" გათვალისწინებით)
		 * 
		 * @param worldToPixelsScale რამდენი პიქსელი შეესაბამება 1 სამყაროს ერთეულს 
		 */
		public convertCenterPixelCoordinatesToWorldCoordinatesWithRespectToWorldOffset(worldToPixelsScale:number):Ivane.Canvas2DHelpers.Point2D
		{
			// ვაკონვერტირებ მაუსის ეკრანულ კოორდინატებს, გასწორებულს ხედის ცენტრზე, 
			// სამყაროს კოორდინატებში რომელზეც შეიქმნება მანქანა.
			this.convertViewPixelCoordinatesToWorldCoordiantes(
				this.inputsManager.mouseXY.x - this.canvasElement.width / 2,
				this.inputsManager.mouseXY.y - this.canvasElement.height / 2,
				1.0 / worldToPixelsScale, 
				this.convertCenterPixelCoordinatesToWorldCoordinatesWithRespectToWorldOffset_Point2D,
				true
			)
			
			return this.convertCenterPixelCoordinatesToWorldCoordinatesWithRespectToWorldOffset_Point2D
		}
		
		/**
		 * მაუსის კოორდინატები სამყაროში
		 */
		private mouseXYWorld:Point2D = new Point2D()
		
		/**
		 * 
		 * ანგარიშობს მაუსის კოორდიანტებს სამყაროს საკოორდინატო სივცეში
		 * 
		 * 
		 */
		convertMouseViewCoordinatesToWorld(worldToPixelsScale)
		{
			this.convertViewPixelCoordinatesToWorldCoordiantes(
				this.inputsManager.mouseXY.x - this.canvasElement.width / 2,
				this.inputsManager.mouseXY.y - this.canvasElement.height / 2,
				1.0 / worldToPixelsScale, 
				this.mouseXYWorld,
				true
			)
			
			return this.mouseXYWorld
		}
		
		
		/**
		 * Canvas2D კოორდინატთა სათავე გადააქვს ხედის ცენტრში.
		 * მაგალიტად: თუ ხედის ზომებია 800x600 სამყაროს (0,0) წერტილი დაჯდება (400,300) ხედის წერტილზე.
		 *
		 */
		centerOriginPointInCanvas()
		{
			this.canvas2DContext.save()
			this.canvas2DContext.translate( Math.ceil(this.canvasElement.width) / 2, Math.ceil(this.canvasElement.height / 2) )
		}
		
		/**
		 * აბრუნებს ცენტრზე გასწორებულ ხედს წინა მდგომარეობაში
		 */
		restoreOriginPoint()
		{
			this.canvas2DContext.restore()
		}
		
		private animationFrameFunction()
		{
			this.deltaTime = this.deltaTimeComputer.getDeltaTimeInSeconds()
			
			
			this.inputsManager.processInput()
			
			

			var shellRestoreTransform:boolean = false

			this.gameStep()
			
			this.inputsManager.postProcessInput()
			
			requestAnimationFrame(()=>{
				this.animationFrameFunction()
			})
			
			
		}
		
		distance2D(x1: number, y1: number, x2: number, y2: number) {
			var deltaX = x2 - x1
			var deltaY = y2 - y1

			return Math.sqrt(deltaX * deltaX + deltaY * deltaY)
		}
			
		/**
		 * კამერის წანაცვლება ხედის ცენტრიდან
		 */
		cameraOffsetInPixels:Point2D = new Point2D()
		enableMiddleMouseButtonDrag:boolean = true
		/**
		 * კოეფიციენტი, 1, სამყაროს ერთეულს რამდენი პიქსელი შეესაბამება
		 */
		pointToPixelForDragginWithMiddleMouseButton:number = 1
		
		dragCameraWithMiddleMouseButton(scale:number):void
		{
			if( this.enableMiddleMouseButtonDrag
				&& this.inputsManager.mouseIsDown
				&& this.inputsManager.mouseButonsBitMap & Ivane.Inputs.MOUSE_BUTTONS.MIDDLE)
			{
				this.worldOffset.x += ( this.inputsManager.mouseDeltaXY.x / this.pointToPixelForDragginWithMiddleMouseButton ) * scale
				this.worldOffset.y += ( this.inputsManager.mouseDeltaXY.y / this.pointToPixelForDragginWithMiddleMouseButton * -1 ) * scale
			}
		}		
			
		gameStep():void
		{
			throw new Ivane.Exceptions.NotImplemetedException()
		}
	}
}

