
module Ivane.IGeom
{
    export interface Point2D
    {
        x:number
        y:number
    }
    
    export interface Circle2D
    {
        center:Point2D
        radius:number
    }
    
}