module Ivane.RPCWS {
    
    /**
     * რეალური პაკეტი რაც იგზავნება ქსელში
     */
    interface RPCPacket {
        func: string;
        params: Object;
    }

    export interface RPCCallback {
        (params: Object, sender: RPCClient): void;
    }

    /**
     * კლიენტ კლასი ivanets_srv.rpcws.RPCServer-ისთვის
     */
    export class RPCClient {
        
        private ws: WebSocket
        private rpcCallbacks: { [index: string]: RPCCallback }

        constructor(remoteAddr: string, remotePort: number) {
            this.rpcCallbacks = {}
            
            this.ws = new WebSocket("ws://"+remoteAddr+":"+remotePort.toString())
            
            this.ws.onerror = (e)=>{
                console.log(e)
            }
            
            this.ws.onmessage = (e)=>{
                this.webSocketMessage(this, e.data)
            }

        }

        private webSocketMessage(client: RPCClient, data: any) {
            var msg = <RPCPacket>JSON.parse(data)

            try {
                this.rpcCallbacks[msg.func](msg.params, client)
            } catch (e) { }
        }        
                
        callRemoteProcedure(func: string, params: Object) {
            var msgString = JSON.stringify(<RPCPacket>{
                    func: func,
                    params: params
            })
            
            this.ws.send(msgString)
        }        
        
        /**
         * ამატებს კლიენტის მიერ გამოძახებად ფუნქციას, მითითებული სახელით.
         */
        setCallback(callbackName: string, callback?: RPCCallback) {
            this.rpcCallbacks[callbackName] = callback
        }

    }

}