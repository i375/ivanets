/// <reference path="../../IvaneMain.ts" />

module Ivane.ThreeJSHelpers {

    
    /**
     * დამხმარე(Gizmo) გეომეტრიის გენერატორ-რენდერერი.
     */
    export class GizmoRenderer
    {
        private bufferedLine:BufferedLinePiecesMesh
        
        private vertexIndex:number = 0
        private vertexCount:number = 0
        
        private positionBuf:THREE.BufferAttribute
        private color4Buffer:THREE.BufferAttribute
        
        private lineColor = vec4.fromValues(1.0, 1.0,1.0,1.0)
        
        /**
         * @param vertexCount წვეროების ბუფერის ზომა.
         */
        constructor(vertexCount:number, color?:number)
        {
            this.bufferedLine = BufferedLinePiecesMesh.createBufferedLines(vertexCount, color ? color : 0xff0000)
            
            this.bufferedLine.lineMesh.frustumCulled = false
            this.initCircleRenderingStructures()
            
            this.positionBuf = this.bufferedLine.getPositionBuffer()
            this.color4Buffer = this.bufferedLine.getColor4Buffer()
            
            this.vertexCount = this.positionBuf.length / this.positionBuf.itemSize
        }
        
        /**
         * წრეწირის წვეროების შემნახველი სტრუქტურა, წვეწირის რენდერისათვის.
         */
        private circVerts:{
            local:Array<GLM.IArray>; // ეტალონური, ერთეულოვანი წრეწირის წვეროები
            world:Array<GLM.IArray>; // სამყაროში არსებული წრეწირის წვეროების ბუფერი
            vec3Buf1:GLM.IArray; //vec3 ტიპის ბუფერი 1
            mat4Buf1:GLM.IArray; //mat4 ტიპის ბუფერი 1
            vertCount:number; // წვეროების რაოდენობა
        } = {
            local : null,
            world : null,
            vec3Buf1 : vec3.create(),
            mat4Buf1 : mat4.create(),
            vertCount : 16
        }
        
        private initCircleRenderingStructures()
        {
            
            //ბუფერების ინიციალიზაცია
            this.circVerts.local = new Array<GLM.IArray>(this.circVerts.vertCount)
            this.circVerts.world = new Array<GLM.IArray>(this.circVerts.vertCount)
            
            for ( var vertIndex_1 = 0; vertIndex_1 < this.circVerts.vertCount; vertIndex_1++ )
            {
                this.circVerts.local[vertIndex_1] = vec3.create()
                this.circVerts.world[vertIndex_1] = vec3.create()
            }
            
            vec3.set( this.circVerts.local[0], 1.0, 0.0, 0.0 )
            
            //ეტალონური წრეწირის, ლოკალური წვეროების გამოთვლა
            var zRotMat4:GLM.IArray = mat4.create()
            
            //ვატრიალებ მატრიცას z ღერძის გარშემო, კუთხით რომელიც წრეწირის წვეროებს შორისაა
            mat4.rotateZ ( zRotMat4, zRotMat4, Math.PI / ( this.circVerts.vertCount / 2 ) ) 
            
            //ყოველი მომდევნო წვერო არის წინა წვერო მოტრიალებული zRotMat4-ით
            for ( var vertIndex_2 = 1; vertIndex_2 < this.circVerts.vertCount; vertIndex_2++ )
            {
                vec3.transformMat4(this.circVerts.local[vertIndex_2], this.circVerts.local[vertIndex_2 - 1], zRotMat4)
            }
            
        }
        
        /**
         * თვლის სამყაროში განლაგებული წრეწირის წვეროებს, იყენებს this.circVerts.local წვეროებს,
         * შედეგს ინახავს this.circVerts.world-ში.
         * 
         * @param radius რადიუსი
         * @param rotationZ მობრუნება z ღერძის გარშემო
         * @param posVec3World {vec3} წრეწირის ცენტრი სამყაროში
         * @return მიმთიტებელი this.circVerts.world-ზე
         */
        private computeCirleWorld(radius:number, rotationZ:number, posVec3World:GLM.IArray):Array<GLM.IArray>
        {
            
            mat4.identity(this.circVerts.mat4Buf1)
            mat4.rotateZ(this.circVerts.mat4Buf1, this.circVerts.mat4Buf1, rotationZ)

            for ( var vertIndex_1 = 0; vertIndex_1 < this.circVerts.vertCount; vertIndex_1++ )
            {
                
                //ვასწორებ რადიუსს 
                vec3.scale(this.circVerts.world[vertIndex_1], this.circVerts.local[vertIndex_1], radius)
                
                //ვატრიალებ წვეროებს
                vec3.transformMat4(this.circVerts.world[vertIndex_1], this.circVerts.world[vertIndex_1], this.circVerts.mat4Buf1)
                
                //ვაადგილებ წვეროებს სამყაროში.
                vec3.add(this.circVerts.world[vertIndex_1], this.circVerts.world[vertIndex_1], posVec3World)
                
            }
            
            return this.circVerts.world
            
        }
                
        
        /**
         * აბრუნებშ renderable სცენის ობიექტს
         */
        getRenderableObject3D():THREE.Line
        {
            return this.bufferedLine.lineMesh
        }
        
        /**
         * ამზადებს ბუფერებს და სხვა ცვლადებს გეომეტრიის გენერაციისათვის.
         */
        begin() : void
        {
            this.vertexIndex = 0
        }
        
        /**
         * 
         * @param rotationZ კუთხე ათვლილი x ღერძიდან, საათის ისრის საწინააღმდეგოდ.
         */
        drawCircle(centerX:number, centerY:number, radius:number, rotationZ:number) : void
        {
            vec3.set(this.circVerts.vec3Buf1, centerX, centerY, 0.0)
            
            var worldCirlceVerts = this.computeCirleWorld(radius, rotationZ, this.circVerts.vec3Buf1)
            
            for ( var vertexIndex_2 = 0; vertexIndex_2 < this.circVerts.vertCount - 1; vertexIndex_2++ ) {
                
                this.drawLine(
                    this.circVerts.world[vertexIndex_2][0], 
                    this.circVerts.world[vertexIndex_2][1],
                    this.circVerts.world[vertexIndex_2 + 1][0], 
                    this.circVerts.world[vertexIndex_2 + 1][1])
            }
            
            this.drawLine(
                this.circVerts.world[0][0], 
                this.circVerts.world[0][1],
                this.circVerts.world[this.circVerts.vertCount - 1][0], 
                this.circVerts.world[this.circVerts.vertCount - 1][1])
                     
        }
        
        drawLine(x1:number, y1:number, x2:number, y2:number) : void
        {
            this.color4Buffer.setXYZW(this.vertexIndex, this.lineColor[0], this.lineColor[1], this.lineColor[2], this.lineColor[3])
            this.setVertexXY(x1,y1)
            
            this.color4Buffer.setXYZW(this.vertexIndex, this.lineColor[0], this.lineColor[1], this.lineColor[2], this.lineColor[3])
            this.setVertexXY(x2,y2)
        }
        
        setLineColor(r:number, g:number, b:number, a:number)
        {
            vec4.set(this.lineColor, r, g, b, a)
        }
        
        drawLine3D(x1:number, y1:number, z1:number, x2:number, y2:number, z2:number) : void
        {
            this.color4Buffer.setXYZW(this.vertexIndex, this.lineColor[0], this.lineColor[1], this.lineColor[2], this.lineColor[3])
            this.setVertexXYZ(x1,y1,z1)
            
            this.color4Buffer.setXYZW(this.vertexIndex, this.lineColor[0], this.lineColor[1], this.lineColor[2], this.lineColor[3])
            this.setVertexXYZ(x2,y2,z2)            
        }
        
        private setVertexXYZ(x:number, y:number, z:number):void
        {
            this.positionBuf.setXYZ(this.vertexIndex, x, y, z)
            this.vertexIndex++
        }
        
        /**
         * პოზიციებსი ბუფერში წერს წვეროს მონაცემებს.
         * 
         * note: რეალიზაციის სიცხადის გამო არის ეს ცალკე ფუნქციად გამოყოფილი, თუმცა
         * ეს ცხადია ეფექტური არაა როცა რამდენიმე ათას წვეროს ეხება საქმე.
         * 
         */
        private setVertexXY(x:number, y:number):void
        {
            this.positionBuf.setXY(this.vertexIndex, x, y)
            this.vertexIndex++
        }
        
        /**
         * კორექტულად ხურავს(ანულებს გამოუყენებელ წვეროებს) ბუფერებს ხატვისათვის.
         */
        finish() : void
        {
            //ბუფერში დარჩენილ ჩაუწერელ წვეროებს ვანულებ, რათა არ გამოჩდნენ ეკრანზე
            for(var vertexIndex_2 = this.vertexIndex; vertexIndex_2 < this.vertexCount; vertexIndex_2++)
            {
                this.positionBuf.setXYZ(vertexIndex_2, 0.0, 0.0, 0.0)
            }
            
            this.positionBuf.needsUpdate = true 
            this.color4Buffer.needsUpdate = true           
        }
    }   
    
}