/// <reference path="../../IvaneMain.ts" />

module Ivane.ThreeJSHelpers {
 
    /**
     * წარმოადგენს THREE.Mesh -ის ტიპს რომელიც არის განკუთვნილი
     * AB, BC, CD, პრინციპით მონაკვეთების ასაღწერად რენდერისათვის.
     */
    export class BufferedLinePiecesMesh 
    {
        static vertexShader:string = 
        `
        attribute vec4 color4;
        varying vec4 vColor;
        void main() {
            vColor = color4;
            gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 ); 

        }
        `
                
        static fragmentShader:string = 
        `
        varying vec4 vColor;

        void main() {

            //gl_FragColor = vec4(1.0,1.0,1.0,0.5);
            gl_FragColor = vColor;

        }        
        `
        
        lineMesh:THREE.Line
        
        static createBufferedLines(vertexCount:number, color:number):BufferedLinePiecesMesh
        {
            var bufferedLine:BufferedLinePiecesMesh = new BufferedLinePiecesMesh()
            var bufferGeometry = new THREE.BufferGeometry()
            
            var vertexArray = new Float32Array(vertexCount * 3)
            var vertexBuffer = new THREE.BufferAttribute(vertexArray, 3)
            
            bufferGeometry.addAttribute("position", vertexBuffer)
            
            var colorArray = new Float32Array(vertexCount * 4)
            var colorBuffer = new THREE.BufferAttribute(colorArray, 4)
            
            bufferGeometry.addAttribute("color4", colorBuffer)
            
            // bufferedLine.lineMesh = new THREE.Line(bufferGeometry, new THREE.LineBasicMaterial(
            //     {
            //         color: color,
            //         transparent: true
                    
            //     }
            // ), THREE.LinePieces)
            
            bufferedLine.lineMesh = new THREE.Line(bufferGeometry, new THREE.ShaderMaterial(
                {
                    fragmentShader: this.fragmentShader,
                    vertexShader: this.vertexShader,
                    // attributes: {
                    //     color4: {type: "vec4"}
                    // },
                    transparent: true
                }), THREE.LinePieces)                        
            
            
            // var LineStrip: LineMode;
            // var LinePieces: LineMode;
                        
            return bufferedLine
        }
        
        /**
         * აბრუნებს პოზიციების ბუფერზე მიმთითებელს.
         */
        getPositionBuffer():THREE.BufferAttribute
        {
            return (<THREE.BufferGeometry>this.lineMesh.geometry).getAttribute("position")
        }
        
        getColor4Buffer():THREE.BufferAttribute
        {
            return (<THREE.BufferGeometry>this.lineMesh.geometry).getAttribute("color4")
        }
    }
       
}