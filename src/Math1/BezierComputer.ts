/*
 * Author Ivane Gegia http://ivane.info
 */

/// <reference path="../../definitions/gl-matrix/gl-matrix.d.ts" />   
/// <reference path="../Types.ts" />

   
module Ivane.Math1
{
	
   
   /**
     * კლასი განკუთვნილია N ხარისხის ბეზიერ წირის წერტილების სათვლელად.
     * 
     */
    export class BezierComputer
    {
        
        /**
         * საკონტროლო წერტილების ბუფერი.
         */
        private controlPoints:Array<GLM.IArray>
        
        /**
         * საკონტროლო წერტილების სამუშაო ასლი.
         */
        private controlPoints_2:Array<GLM.IArray>
        
        private insertedContrPointsCount:number = 0
        
        /**
         * სამ ელემენტიანი ვექტორი
         * vec3
         */
        private bezierPoint:GLM.IArray
        
        constructor(controlPointsBufferSize:number)
        {
            
            this.initControlPointsBuffer(controlPointsBufferSize)   
            this.bezierPoint = vec3.create()
            
        }
        
        private initControlPointsBuffer(pointsNumber:number)
        {
            
            this.controlPoints = new Array<GLM.IArray>(pointsNumber)
            this.controlPoints_2 = new Array<GLM.IArray>(pointsNumber)
            
            for ( var pointIndex = 0; pointIndex < pointsNumber; pointIndex++ )
            {
                
                this.controlPoints[pointIndex] = vec3.create()
                this.controlPoints_2[pointIndex] = vec3.create()
                
            }
            
        }
        
        /**
         * 
         * ამატებს საკონტროლო წერტილს ბუფერში.
         * იღებს როგორც 2 ელემენტიან ისე 3 ელემენტიან.
         * 
         */                
        pushControlPoint(x:number, y:number, z?:number)
        {
            
            vec3.set( this.controlPoints[this.insertedContrPointsCount], x, y, z ? z : 0.0 )
            
            this.insertedContrPointsCount++
            
        }
        
        private copyControlPointsToBuffer2()
        {
            
            for ( var controlPoint = 0; controlPoint < this.insertedContrPointsCount; controlPoint++ )
            {
                vec3.copy( this.controlPoints_2[controlPoint], this.controlPoints[controlPoint] )
            }
            
        }
        
        /**
         * 
         * თვლის ბეზიეს წერტილს მითითებული t-თვის,
         * ბეზიეს მრუდის ხარისხი უდრის საკონტროლო წერტილების ოდენობას.
         * 
         * @return vec3 სამ ელემენტიან ვექტორს 
         */
        computeBezierPoint(t:number):GLM.IArray
        {
            
            if(this.insertedContrPointsCount == 1)
            {
                vec3.copy(this.bezierPoint, this.controlPoints[0])
                
                return this.bezierPoint
            }            
            
            this.copyControlPointsToBuffer2()
            
            // დათვლილი საკონტროლო წერტილების რაოდენობა
            // ეს რიცხვი  
            var contrPointsComputed = this.insertedContrPointsCount
            
            while( contrPointsComputed > 1 )
            {
                
                var contrPointsComputed_2 = 0
                
                for ( var controlPointIndex = 0; controlPointIndex < contrPointsComputed - 1; controlPointIndex++ )
                {
                    
                    vec3.lerp(
                        this.controlPoints_2[ controlPointIndex ], 
                        this.controlPoints_2[ controlPointIndex ],
                        this.controlPoints_2[ controlPointIndex + 1 ],
                        t)
                        
                    contrPointsComputed_2 ++
                }   
                
                contrPointsComputed = contrPointsComputed_2             
                       
            }
            
            vec3.copy(this.bezierPoint, this.controlPoints_2[0])
            
            return this.bezierPoint
            
        }
        
        //ველები არის computeLength-ის მიერ გამოყენებისთვის
        private t:number = 0
        private prevBezierPoint = vec3.create()
        private length = 0
        
        /**
         * მეთოდი თვლის ბეზიეს მრუდის სიგრძეს მითითებული ბიჯის სიზუსტით.
         * 
         * @param deltaT t ბიჯის ზომა რომესტაც მეთოდი გამოიყენებს მრუდის წერტილების გადასასთვლელად რომელთა შორის მანძილებსაც აჯამავს.
         */
        computeLength(deltaT:number):number
        {
            this.t = 0
            this.length = 0
            
            //ვითვლი პირველ 0 წერტილს და ვინახავ წინა წერტილის ველში
            this.computeBezierPoint(this.t)
            vec3.copy(this.prevBezierPoint, this.bezierPoint)
            
            while(true)
            {
                
                this.t = this.t + deltaT
                
                if(this.t > 1)
                {
                    this.t = 1
                }                
                
                this.computeBezierPoint(this.t)
                
                //მანძილს წინა წერტილსა და მიმდინარეს შორის ვუმატებ ჯამური სიგრძის შემნახველ ველს
                this.length = this.length + vec3.distance(this.prevBezierPoint, this.bezierPoint)

                vec3.copy(this.prevBezierPoint, this.bezierPoint)
                
                if(this.t == 1)
                {
                    break
                }
                
            }
            
            return this.length
        }
        
        private tangentVector:GLMVec3 = vec3.create()
        
        //computeTangentVectorAt გამოყენებისთვის
        private tangentT:number = 0
        
        /**
         * თვლის t წერტილში მხების ვექტორს.
         * 
         * @param t 
         * @param tDelta t-ს delta მიდამო, t-tDelta და t+tDelta რომელზეც აიგება მხები.
         */
        computeTangentVectorAt(t:number, tDelta:number):GLMVec3
        {
            this.tangentT = t - tDelta
            
            if (this.tangentT < 0) this.tangentT = 0
            
            this.computeBezierPoint(this.tangentT)
            
            vec3.copy(this.prevBezierPoint, this.bezierPoint)
            
            this.tangentT = t + tDelta
            
            if (this.tangentT > 1) this.tangentT = 1
            
            this.computeBezierPoint(this.tangentT)
            
            vec3.subtract(this.tangentVector, this.bezierPoint, this.prevBezierPoint)
            
            return this.tangentVector
        }
        
        reset()
        {
            
            this.insertedContrPointsCount = 0
            
        }
        
    }
    
}