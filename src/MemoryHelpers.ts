/*
 * Author Ivane Gegia http://ivane.info
 */

module Ivane.Utils
{
	
    /**
     * ქმნის გადაცემული ტიპის ობიექტების მასივს.
     * 
     * @param arrayObjectClass ტიპი რითიც უნდა შეივსოს მასივი
     * @param itemsCount მასივის ელემენტების რაოდენობა
     */
	export function createArray(arrayObjectClass:any, itemsCount:number):Array<any>
	{
		var arr = new Array()
		
		for(var arrayIndex = 0; arrayIndex < itemsCount; arrayIndex++)
		{
			arr[arrayIndex] = new (<any>arrayObjectClass)()
		}
		
		return arr
	}
	
}