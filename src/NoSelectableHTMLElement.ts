/*
 * Author Ivane Gegia http://ivane.info
 */
    
module Ivane.DOMHelpers { 

    /**
     * მითითებული css კლასის მქონე ელემენტებზე აუქმებს მაუსით მონიშვნის ფუნქციონალს, გამოსადეგია UI ელემენტებისათვის.
     */
    export function makeHTMLElementsNotSelectableByClassName(className:string):void {
        
        var notSelectables = document.getElementsByClassName(className);
        
        for (var notSelectableIndex = 0; notSelectableIndex < notSelectables.length; notSelectableIndex++) {
            var notSelectable = <any>notSelectables[notSelectableIndex];
            notSelectable.style["-moz-user-select"] = "none";
            notSelectable.style["-webkit-user-select"] = "none";
            notSelectable.style["-ms-user-select"] = "none";
            notSelectable.style["user-select"] = "none";
            notSelectable.style["-o-user-select"] = "none";
            notSelectable.style["cursor"] = "default";
            notSelectable.setAttribute("unselectable", "on");
            notSelectable.setAttribute("onselectstart", "return false");
            notSelectable.setAttribute("onmousedown", "return false");
        }
        
    }

}