/*
 * Author Ivane Gegia http://ivane.info
 */

///<reference path="../definitions/threejs/three.d.ts" />
///<reference path="DeltaTime.ts"/>
///<reference path="CanvasInputsManager.ts"/>
///<reference path="ThreeJSHelpers/ThreeJSHelpers.ts"/>
/// <reference path="../definitions/gl-matrix/gl-matrix.d.ts" />


module Ivane.GameClasses
{

    /**
     * ზოგადი სათამაშო კლასი, რომელსაც აქვს მინიმალური ფუნქციონალი
     * არ აქვს გასაზღვრული გრაფიკული კონტექსტის ტიპი, 2d/3d
     */
	export class GameClass
	{
        
		canvasElement:HTMLCanvasElement
		
		deltaTime:number
		inputsManager:Ivane.Inputs.CanvasInputsManager	

		private deltaTimeComputer:Ivane.Time.DeltaTimeComputer
		private enableMiddleMouseCameraDrag:boolean = false
		private initCalled:boolean = false
		
		private canvasCenterX:number
		private canvasCenterY:number
										
		initCanvas2D(width:number, height:number, canvasContainer:HTMLElement):void
		{
			var viewRatio = width / height
			
            
			this.canvasElement = <HTMLCanvasElement>document.createElement("canvas")
			this.canvasElement.width = width
			this.canvasElement.height = height	
            						
			canvasContainer.appendChild(this.canvasElement)

			this.deltaTimeComputer = new Ivane.Time.DeltaTimeComputer()
			this.deltaTime = this.deltaTimeComputer.getDeltaTimeInSeconds()
			
			this.inputsManager = new Ivane.Inputs.CanvasInputsManager()
			this.inputsManager.startProcessingInputFor( this.canvasElement )
			
			this.initCalled = true
			
            this.computeCanvasCenterXY()
		}
        
        computeCanvasCenterXY():void
        {
            
			this.canvasCenterX = this.canvasElement.width / 2
			this.canvasCenterY = this.canvasElement.height / 2
                        
        }
				
		run():void
		{
			if(this.initCalled == false)
			{
				console.error("GameClassThreeJS.initWithOrthoCamera was not called!")	
				return
			}
			
			this.animationFrameFunction()
		}	
						
		private animationFrameFunction()
		{
			this.deltaTime = this.deltaTimeComputer.getDeltaTimeInSeconds()
			
			this.inputsManager.processInput()
			
			var shellRestoreTransform:boolean = false
            
            this.getMouseXYInGLView()

			this.gameStep()
			
			this.inputsManager.postProcessInput()
			
			requestAnimationFrame(()=>{
				this.animationFrameFunction()
			})
            
		}
        
        /**
         * @return true თუ კლავიშას მიმდინარე კადრში დაეჭირა.
         */
        keyDown(keyCode:Ivane.Inputs.KeyCodes)
        {
            return this.inputsManager.keyWasDown(keyCode) == false && this.inputsManager.keyIsDown(keyCode)
        }
        
        /**
         * @return true თუ კლავიშას მიმდნარე კადრში აეშვა ხელი.
         */
        keyUp(keyCode:Ivane.Inputs.KeyCodes)
        {
            return this.inputsManager.keyWasDown(keyCode) == true && this.inputsManager.keyIsDown(keyCode)
        }
        
        
        /**
         * vec3
         * 3 კომპონენტიანია ტექნიკური მიზეზებით
         * 
         */
        private _mouseXYInGLView:GLM.IArray = vec3.create()
		
        /**
         * გადაყავს მაუსის პიქსეული კოორდინატები, gl ხედის კოორდინატებში.
         * 
         * შედეგები ინახება _mouseXY-ში
         * 
         * @return GLM.IArray
         *  */	
        getMouseXYInGLView(x?:number, y?:number):GLM.IArray
        {
            this.computeCanvasCenterXY()
            
            if (x == undefined) {
                this._mouseXYInGLView[0] = (this.inputsManager.mouseXY.x - this.canvasCenterX) / this.canvasCenterX
                this._mouseXYInGLView[1] = - (this.inputsManager.mouseXY.y - this.canvasCenterY) / this.canvasCenterY
                this._mouseXYInGLView[2] = 0.0
            }
            else {
                this._mouseXYInGLView[0] = (x - this.canvasCenterX) / this.canvasCenterX
                this._mouseXYInGLView[1] = - (y - this.canvasCenterY) / this.canvasCenterY
                this._mouseXYInGLView[2] = 0.0
            }
 
            return this._mouseXYInGLView
            
        }
            
		gameStep():void
		{
			throw new Ivane.Exceptions.NotImplemetedException()
		}
	}
}

