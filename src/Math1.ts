/*
 * Author Ivane Gegia http://ivane.info
 */

/// <reference path="Math1/BezierComputer.ts" />


module Ivane.Math1
{
	
	export function random (min, max) {
        
			return Math.floor(Math.random() * (max - min + 1) + min);
            
	}
    
    const DEGREES = 180 / Math.PI
	
	export function radians2degrees(radians:number):number
	{
		return radians * DEGREES
	}
	
	export function degrees2radians(degrees:number):number
	{
		return degrees / DEGREES
	}
    
 
			
}