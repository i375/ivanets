/// <reference path="../../IvaneMain.ts" />
/// <reference path="Camera.ts" />


module Ivane.WebGL
{

	/**
	 * აღწერს კამერის ობიექტს
	 */
	export class Camera2D extends Camera
	{
                
		/**
		* ხედის მასშტაბირების აღმწერი ვექტორი
		*/
		private viewScaleVector:GLM.IArray
		
		/**
		* ხედის მასშტაბირების ცენტრალური პარამეტრი
		*/
		viewScale:number
		
		private viewRotationVector:GLM.IArray
		
		/**
		* ხედის Z ღერძის გარშემო ტრიალის კოეფიციენტი
		*/
		viewRotation:number		
		
		private viewRatio:number = 0.0
		
		/**
		 * კამერის გადაადგილების ვექტორი(ნიშან შებრუნებული კამერის პოზიციის ვექტორი)
		 */
		private translationVector:GLM.IArray
		
		constructor()
		{
            super()
            
			this.cameraMatrix = mat4.create()
			mat4.identity(this.cameraMatrix)
			
			this.viewScaleVector = vec3.create()
			vec3.set(this.viewScaleVector, 1.0, 1.0, 1.0)
			
			this.viewScale = 1.0
			
			this.viewRotationVector = vec3.create()
			
			this.translationVector = vec3.create()			
		}
		
		private resetViewMatrix()
		{
			mat4.identity(this.cameraMatrix)
		}
		
		/**
		 * ცვლის კამერის პოზიციას სამყაროში
		 */
		setPosition(x:number, y:number)
		{	
			this.resetViewMatrix()
			vec3.set(this.translationVector, -x, -y, 0.0)
			
			this._setScale()			
			this._setPosition()
			
			this.computeInvertedViewMatrix()
		}	
		
		private _setPosition()
		{
			mat4.translate(this.cameraMatrix, this.cameraMatrix, this.translationVector)
			
		}	
		
		/**
		 * საზღვრავს კამერის მასშტაბირებას.
		 * მასშტაბირებისას გამოიყენება ხედის რეზოლუციების შეფარდება იმისთვის რომ სურათი არ დამახიჯდეს
		 * არაკვარდატული ხედის შემთხვევაში.
		 */
		setScale(scale:number)
		{
			this.resetViewMatrix()
			this.viewScale = scale			
			
			this._setScale()						
			this._setPosition()
			
			this.computeInvertedViewMatrix()
		}
				
		private _setScale()
		{
			vec3.set(this.viewScaleVector, this.viewScale / this.viewRatio, this.viewScale, 1.0)
			mat4.scale(this.cameraMatrix, this.cameraMatrix, this.viewScaleVector)		
		}
		
		private computeInvertedViewMatrix()
		{
			mat4.invert(this.viewMatrixInverted, this.cameraMatrix)
		}
		
		/**
		 * საზღვრავს ხედის რეზოლუციის შეფარდებას
		 * 
		 * @param canvasWidthDividedByHeight canvas.width / canvas.height
		 */
		setViewRatio(canvasWidthDividedByHeight:number)
		{
			this.viewRatio = canvasWidthDividedByHeight
		}
		
		viewToWorld(out:GLM.IArray, viewPoint:GLM.IArray)
		{
			vec3.transformMat4(out, viewPoint, this.viewMatrixInverted)
		}
		
		/**
		 * აბრუნებს ხედის მატრიცას რომელიც გამოიყენება gl შეიდერში
		 */
		getCameraMatrix():GLM.IArray
		{
			return this.cameraMatrix
		}
		
		getCameraMatrixInverted():GLM.IArray
		{
			return this.viewMatrixInverted
		}
        
	}	
}




