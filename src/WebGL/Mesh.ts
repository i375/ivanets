/// <reference path="../../IvaneMain.ts" />
/// <reference path="Geometry.ts" />
/// <reference path="Transform3D.ts" />
/// <reference path="GLBuffer.ts" />
/// <reference path="Object3D.ts" />



module Ivane.WebGL
{
	
	export class AttributeBuffers
	{
		positionBuffer:GLBuffer = null
		colorBuffer:GLBuffer = null
		uv1Buffer:GLBuffer = null
		elementsBuffer:GLBuffer = null
	}
	
	/**
	 *საბოლოო ჯამში ამან უნდ შეინახოს გეომეტრია და შეიდერი რომლითაც უნდა
	 * დაიხატოს გეომეტრია, ასევე ასოცირებული ტექსტურა და სხვა დეტალები
	 */
	export class Mesh extends Object3D
	{

		attributeBuffers:AttributeBuffers

		glDrawMode:number

		/**
		 * გეომეტრიის შემნახველი
		 */
		geometry:Geometry
		

		
		/**
		 * თუ geometry-ს ტრანსფორმაცია განხორციელდება CPU-ზე, worldMatrix4-ს გამოყენებით
		 * მაშინ ეს ველი შეინახავს ტრანსფორმირებულ წვეროებს.
		 */
		worldGeometry:Geometry
		
		gl:WebGLRenderingContext
        
        shader:Shader
		
		/**
		 * აინიცალიზირებს საჭირო გეომეტრიის ბუფერეს თუ წვეროების რაოდენობა მითითებულია
		 * 
		 * @param geometry საწყისი გეომეტრია, რომლის ტრასფორმაციაც ან GPU-ზე ან CPU(საჭიროებს worldGeometry-ის)-ზე მოხდება
		 * @param worldGeometry ინახავს computeWorldGeometryOnCPU მიერ geometry ტრანსფორმაციის შედეგებს
		 */
		constructor(glContex:WebGLRenderingContext, shader:Shader, geometry:Geometry, worldGeometry?:Geometry)
		{
			super()
            
			this.gl = glContex
            this.shader = shader
			this.geometry = geometry
			this.attributeBuffers = new AttributeBuffers()
			
			if(worldGeometry)
			{
				this.worldGeometry = worldGeometry	
			}
				
		}
		
		/**
		 * დროებითი ცვლადი
		 */
		private vertex3f = vec3.create()
		
		
		/**
		 * აღნიშნავს რომ გეომეტრია დათვლილია CPU-ზე და GPU-ში შეიძლება გაიგზავნოს worldGeometry და არა geometry
		 */
		private _worldGeometryIsComputedOnCPU:boolean = false
		
		worldGeometryIsComputedOnCPU():boolean
		{
			return this._worldGeometryIsComputedOnCPU
		}
				
		/**
		 * worldMatrix4-ის გამოყენებით გარდაქმნის geometry-ში არსებულ წვეროებს და ინახავს მათ worldGeometry
		 */
		transformOnCPU()
		{
			const vertexCount = this.geometry.attributes.position.getElementsCount()
			
			for(var vertexNumber = 0; vertexNumber < vertexCount; vertexNumber++)
			{
				// მოვიპოვებ გეომეტრიის წვეროს
				this.geometry.attributes.position.getElement3f(this.vertex3f, vertexNumber)
				
				// ვახდენ წვეროს ტრანსფორმაციას
				vec3.transformMat4(this.vertex3f, this.vertex3f, this.transform3d.transformMat4)
				
				// ვინახავს გარდაქმნილ წვეროს worldGeometry-ში
				this.worldGeometry.attributes.position.setElement3f(vertexNumber, this.vertex3f[0], this.vertex3f[1], this.vertex3f[2])
				
			}
			
			// ვაფიქსირებ რომ worldGeometry გამოთვლილია და მზადაა გამოყენებსითვის GPU-ზე
			this._worldGeometryIsComputedOnCPU = true
		
		}
		
        updatePositionsOnGPU():void
        {
            if(this.attributeBuffers.positionBuffer == null)
            {
                this.attributeBuffers.positionBuffer = new GLBuffer(this.gl, GLBufferTypes.ARRAY_BUFFER)
            }
            
            if(this._worldGeometryIsComputedOnCPU)
            {
                this.attributeBuffers.positionBuffer.updateBufferDataForDynamicDraw(this.worldGeometry.attributes.position.buffer)
            }
            else
            {
                this.attributeBuffers.positionBuffer.updateBufferDataForDynamicDraw(this.geometry.attributes.position.buffer)    
            }
               
        }
        
		updateColorsGPU():void
		{
            if(this.attributeBuffers.colorBuffer== null)
            {
                this.attributeBuffers.colorBuffer = new GLBuffer(this.gl, GLBufferTypes.ARRAY_BUFFER)
            } 
            
			this.attributeBuffers.colorBuffer.updateBufferDataForDynamicDraw(this.geometry.attributes.color.buffer)
		}
			
		updateUV1OnGPU():void
		{   
            if(this.attributeBuffers.uv1Buffer == null)
            {
                this.attributeBuffers.uv1Buffer = new GLBuffer(this.gl, GLBufferTypes.ARRAY_BUFFER)
            }
            
            this.attributeBuffers.uv1Buffer.updateBufferDataForDynamicDraw(this.geometry.attributes.uv1.buffer)
		}
        
        updateElementsOnGPU():void
        {
            if(this.attributeBuffers.elementsBuffer == null)
            {
                this.attributeBuffers.elementsBuffer = new GLBuffer(this.gl, GLBufferTypes.ELEMENT_ARRAY_BUFFER)
            }
            
            this.attributeBuffers.elementsBuffer.updateBufferDataForDynamicDraw(this.geometry.attributes.elements)
        }
		
	}
	
}
