/// <reference path="../../IvaneMain.ts" />
/// <reference path="VertexAttributeArray.ts" />

/**
 * აღწერს WebGL-ის ბუფერს GPU-ზე
 */
module Ivane.WebGL
{
    
    export enum GLBufferTypes
    {
        ARRAY_BUFFER,
        ELEMENT_ARRAY_BUFFER
    }
    
    export class GLBuffer
    {
        
        private gl:WebGLRenderingContext
        private glBuffer:WebGLBuffer
        private glBufferTarget:number
    
        /**
         * @param target gl.ARRAY_BUFFER ან gl.ELEMENT_ARRAY_BUFFER
         */
        constructor(glContext:WebGLRenderingContext, target:GLBufferTypes)
        {
            this.gl = glContext
            
            this.glBuffer = this.gl.createBuffer()
            
            if(target == GLBufferTypes.ARRAY_BUFFER)
            {
                this.glBufferTarget = this.gl.ARRAY_BUFFER
            }    
            
            if(target == GLBufferTypes.ELEMENT_ARRAY_BUFFER)
            {
                this.glBufferTarget = this.gl.ELEMENT_ARRAY_BUFFER
            }
        }
        
        updateBufferDataForDynamicDraw(data:ArrayBuffer):void
        {
            this.gl.bindBuffer(this.glBufferTarget, this.glBuffer)
            this.gl.bufferData(this.glBufferTarget, data, this.gl.DYNAMIC_DRAW)
        }
        
        updateBufferDataForStaticDraw(data:ArrayBuffer):void
        {
            this.gl.bindBuffer(this.glBufferTarget, this.glBuffer)
            this.gl.bufferData(this.glBufferTarget, data, this.gl.STATIC_DRAW)
        }
        
        bindBufer():void
        {
            this.gl.bindBuffer(this.glBufferTarget, this.glBuffer)
        }
        
    }
    
}