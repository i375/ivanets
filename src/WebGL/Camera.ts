/// <reference path="../../IvaneMain.ts" />


module Ivane.WebGL
{

    export class Camera
    {

		/**
		* საბოლოოდ(opengl-ში) გამოყენებადი მატრიცა
		*/
		cameraMatrix:GLM.IArray
		
		viewMatrixInverted:GLM.IArray = mat4.create()   
        
                
    }
    
}