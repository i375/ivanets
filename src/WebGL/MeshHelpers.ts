/// <reference path="../../IvaneMain.ts" />
/// <reference path="Geometry.ts" />
/// <reference path="Transform3D.ts" />
/// <reference path="GLBuffer.ts" />
/// <reference path="Object3D.ts" />



module Ivane.WebGL
{
    /**
     * კლასი რომელიც შეიცავს სტატიკურ მეთოდებს და არის
     * {Mesh} ტიპის ობიექტის შექმნის დამხმარე ფუნქციების კოლექცია.
     */
    export class MeshHelpers extends Mesh
    {
        
        /**
		 * ქმნის 2D კვადრატს.
		 */
		static createQuadMesh(glContext:WebGLRenderingContext, shader:Shader, width?:number, height?:number):Mesh
		{
			var mesh = new Mesh(glContext, shader, new Geometry(4, true, 6), new Geometry(4, false, 0))
			
			var _width = 1.0 / 2.0 
			var _height = 1.0 / 2.0
			
			if(width)
			{
				_width = width / 2
			}
			
			if(height)
			{
				_height = height / 2
			}
			
            mesh.geometry.attributes.position.setElement3f(0, _width,  _height,  0.0)
            mesh.geometry.attributes.position.setElement3f(1,-_width,  _height,  0.0)
            mesh.geometry.attributes.position.setElement3f(2,-_width, -_height,  0.0)
            mesh.geometry.attributes.position.setElement3f(3, _width, -_height,  0.0)

			mesh.geometry.attributes.color.setElement4f(0, 0.5, 0.2, 0.5, 0.5)
			mesh.geometry.attributes.color.setElement4f(1, 0.5, 0.2, 0.5, 0.5)
			mesh.geometry.attributes.color.setElement4f(2, 0.5, 0.2, 0.5, 0.5)
			mesh.geometry.attributes.color.setElement4f(3, 0.5, 0.2, 0.5, 0.5)	
			
			mesh.geometry.attributes.elements[0] = 0
			mesh.geometry.attributes.elements[1] = 1
			mesh.geometry.attributes.elements[2] = 2
			mesh.geometry.attributes.elements[3] = 0
			mesh.geometry.attributes.elements[4] = 2
			mesh.geometry.attributes.elements[5] = 3
			
			mesh.geometry.attributes.uv1.setElement2f(0, 1.0, 1.0)
			mesh.geometry.attributes.uv1.setElement2f(1, 0.0, 1.0)
			mesh.geometry.attributes.uv1.setElement2f(2, 0.0, 0.0)
			mesh.geometry.attributes.uv1.setElement2f(3, 1.0, 0.0)
			
			mesh.updatePositionsOnGPU()
			mesh.updateColorsGPU()
			mesh.updateUV1OnGPU()
			mesh.updateElementsOnGPU()			
			
			return mesh
		}      
          
    }
}