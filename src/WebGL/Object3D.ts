/// <reference path="../../IvaneMain.ts" />
/// <reference path="Geometry.ts" />
/// <reference path="Transform3D.ts" />
/// <reference path="GLBuffer.ts" />

module Ivane.WebGL
{
	
    export class Object3D
    {
		/**
		 * მატრიცა რომელიც ინახავს geometry-ს ტრანსფორმაციას სამყაროში
		 * რომცლიეც პოტენციურად შეიძლება შეინახოს worldGeometry-ში თუ 
		 * ტრანსფორმაცია განხორციელდება CPU-ზე.
		 */
		transform3d:Transform3D
        
        /**
         * შვილობილი ობიექტების მასივი.
         * არ არის გათვლიილ პირდაპირ რედაქტირებაზე. 
         * აქვს public წვდომა მხოლოდ {Renderer} კლასისთვის.
         */
        children:Array<Object3D>
        
        /**
         * მშობელი ობიექტი სცენის იერარქიაში.
         */
        parent:Object3D = null
        
        /**
         * ტექსტური სახელი, მაგალითად "სკამი", "ბორბალი", ...
         */
        name:string = ""
        
        constructor()
        {            
            this.transform3d = new Transform3D()
            
            this.children = new Array<Object3D>()
        }
        
        /**
         * ამატებს შვილობის ობიექტს.
         * რენდერისას, შვილობილი ობიექტის transform3d ითვლიბა მოცემული 
         * ობიექტის transform3d-სთან მიმართებით. ასე შემდეგ ბოლომდე.
         */
        addChild(obj:Object3D):void
        {
            this.children.push(obj)
            obj.parent = this
        }
        
        /**
         * სლის შვილობილ ობიექტ obj შვლების სიიდან.
         */
        removeChild(obj:Object3D):void
        {
            var childObjectIndex = -1
            
            for(var counter =  0; counter < this.children.length; counter++)
            {
                
                if(this.children[counter] == obj)
                {
                    childObjectIndex = counter
                    break
                }
                
            }
                        
            if(childObjectIndex >=0)
            {
                obj.parent = null
                this.removeChildAtIndex(childObjectIndex)    
            }
             
        }
                        
        /**
         * შვილობილი ობიექტის წაშლა ხდება index-ით.
         */
        removeChildAtIndex(index:number):void
        {
            this.children.splice(index, 1)
        }
                
        /**
         * მოძებნის შვილ ობიექტს არის თუ არა ობიექტის მითითებული სახელით და დააბრუნებს მას.
         * 
         * @param name საძიებელი ობიექტის სახელი.
         */
        getChildByName(name:string):Object3D
        {
            var childWithName = null
            
            for(var childIndex = 0; childIndex < this.children.length; childIndex++)
            {
                if(this.children[childIndex].name == name)
                {
                    childWithName = this.children[childIndex]
                    break
                }
            }    
            
            return childWithName
        }
        
        /**
         * მოძებნის შვილ ობიექტს გაართიანებს რომელთაც აქვთ მითითებული სახელი,
         * გააერთიანებს მათ მასივში და დააბრუნებს.
         * 
         * @param name საძიებელი ობიექტების სახელი
         */
        getChildrenWithName(name:string):Array<Object3D>
        {
            
            var childrenWithName = new Array<Object3D>()
            
            for(var childIndex = 0; childIndex < this.children.length; childIndex++)
            {
                if(this.children[childIndex].name == name)
                {
                    childrenWithName.push(this.children[childIndex])
                }
            }    
            
            return childrenWithName            
            
        }
        
    }
    
}