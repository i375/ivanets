/// <reference path="../../IvaneMain.ts" />


module Ivane.WebGL
{
	export class TextureBackedFramebuffer
	{
		private gl:WebGLRenderingContext
		
		/**
		 * სიღრმის ბუფერი
		 */
		private depthBuffer:WebGLRenderbuffer
		
		/**
		 * ფერების შემნახველი ტექსტურა
		 */
		private renderTexture:WebGLTexture
		
		width:number = 0
		height:number = 0
		
		/**
		 * თვით კადრის ბუფერი
		 */
		private frameBuffer:WebGLFramebuffer
		
		constructor(glContext:WebGLRenderingContext, width:number, height:number)
		{
			this.gl = glContext
			
			this.width = width
			this.height = height
			
			this.frameBuffer = this.gl.createFramebuffer()
			this.bindFramebuffer()
			
			this.renderTexture = this.gl.createTexture()
			this.gl.bindTexture(this.gl.TEXTURE_2D, this.renderTexture)
			
			//ვაფიქსირებ რომ ტექსტურა y ღერძზე უნდა აყირავდეს რადგან რენდერის შედეგები ამოტრიალებულ y სივრცეში ინახება
			this.gl.pixelStorei(this.gl.UNPACK_FLIP_Y_WEBGL, <any>true)
			
			this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR)
			this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR)
			//this.gl.generateMipmap(this.gl.TEXTURE_2D)
						
			//ვავსებ ტექსტურას "ცარიელი" მონაცემებით
			this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, width, height, 0, this.gl.RGBA, this.gl.UNSIGNED_BYTE, null)
			
			//კადრის ბუფერზე ვამაგრებ სიღრმის ბუფერს
			this.depthBuffer = this.gl.createRenderbuffer()
			this.gl.bindRenderbuffer(this.gl.RENDERBUFFER, this.depthBuffer)
			this.gl.renderbufferStorage(this.gl.RENDERBUFFER, this.gl.DEPTH_COMPONENT16, width, height)
			
			//კადრის ბუფერზე ვამაგრაებ ფერების ბუფერს
			this.gl.framebufferTexture2D(this.gl.FRAMEBUFFER, this.gl.COLOR_ATTACHMENT0, this.gl.TEXTURE_2D, this.renderTexture, 0)
			this.gl.framebufferRenderbuffer(this.gl.FRAMEBUFFER, this.gl.DEPTH_ATTACHMENT, this.gl.RENDERBUFFER, this.depthBuffer)
			
			//ვათავისუფლებ მიმდინარე აქტიურ ობიექტებს აქტიური მდგომარეობიდან
			this.gl.bindTexture(this.gl.TEXTURE_2D, null)
			this.gl.bindRenderbuffer(this.gl.RENDERBUFFER, null)
			
			this.bindMainFramebuffer()
			
		}
		
		bindFramebuffer()
		{
			this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.frameBuffer)
		}
		
		/**
		 * ძირითადი ფრეიმ ბუფერის აქტივაცია
		 */
		bindMainFramebuffer()
		{
			this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null)	
		}
		
		/**
		 * 
		 * ააქტიურებს ტექსტურას, მითითებულ GL texture unit-ში
		 * gl.TEXTURE0, gl.TEXTURE1 ...
		 * 
		 */
		useRenderTexture(textureUnit?:number)
		{
			
			var texUnit = this.gl.TEXTURE0
			
			if(textureUnit)
			{
				texUnit = textureUnit
			}
			
			//ვააქტიურებს ტექსტურის სლოტს
			this.gl.activeTexture(texUnit)
			this.gl.bindTexture(this.gl.TEXTURE_2D, this.renderTexture)
			
		}		
		
	}
}