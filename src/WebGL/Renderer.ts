/// <reference path="../../IvaneMain.ts" />
/// <reference path="Geometry.ts" />
/// <reference path="Transform3D.ts" />
/// <reference path="Scene.ts" />



module Ivane.WebGL
{
    
    export class Renderer
    {
        gl:WebGLRenderingContext
        
        constructor(glContext:WebGLRenderingContext)
        {
            this.gl = glContext
        }
        
        // setClearColor(colorHEX:number):void
        // {
        //     this.gl.clearColor()
        // }
        
        /**
         * ასუფთავებს ფერის ბუფერს
         */
        clearColorBuffer():void
        {
            this.gl.clear(this.gl.COLOR_BUFFER_BIT)
        }
        
        /**
         * ასუფთავებს სიღრმის ბუფერს.
         */
        clearDepthBuffer():void
        {
            this.gl.clear(this.gl.DEPTH_BUFFER_BIT)
        }
        
        /**
         * ასუფთავებს ფერის და სიღრვმის ბუფერს.
         */
        clearColorAndDepthBuffers():void
        {
            this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT)
        }
        
        /**
         * რენდერ ფუნქცია.
         */
        render(renderable:(Object3D|Scene|Mesh), camera:Camera):void
        {
            throw new Ivane.Exceptions.NotImplemetedException()   
        }
        
    }
	
}