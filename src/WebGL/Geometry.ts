/// <reference path="../../IvaneMain.ts" />
/// <reference path="VertexAttributeArray.ts" />


module Ivane.WebGL
{
	export class GeometryAttributes
	{
		/**
		 * Array<vec3f> ტიპის ბუფერი წვეროებისათვის
		 */		
		position:VertexAttributeArray
		
		color:VertexAttributeArray
		
		uv1:VertexAttributeArray
		
		/**
		 * წვეროების ინდექსების ბუფერი
		 */		
		elements:Uint32Array
		
		constructor()
		{
			this.position = null
			this.color = null
			this.elements = null
		}
	}
    
	/**
	 * ინახავს წვეროებით აღწერის გეომეტრიას.
	 * წვეროები ყოველთვის vec3 ტიპისაა ბუფერში.
	 */
	export class Geometry
	{
        
		attributes:GeometryAttributes = new GeometryAttributes()
						
		/**
		 * @param vertexCount წვეროების რაოდენობა
		 * @param vertexIndexBufferSize წვეორების ინდექსების რაოდენობა
		 */
		constructor(vertexCount:number, enableVertexColors:boolean, elementCount:number)
		{
            
			this.attributes.position = createVertexPositionArray3f(vertexCount)
			
			if(enableVertexColors)
			{
				this.attributes.color = createVertexColorArray4f(vertexCount)
			}
			
			if(elementCount > 0)
			{
				this.attributes.elements = new Uint16Array(elementCount)
			}
			
			this.attributes.uv1 = createVertexUVArray2f(vertexCount)
            
		}

		
		
	}		
}