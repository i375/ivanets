/// <reference path="../../IvaneMain.ts" />
/// <reference path="Texture2D.ts" />
/// <reference path="TextureBackedFramebuffer.ts" />


module Ivane.WebGL
{

	interface UniformsDictionary
	{
		[name: string]: number | WebGLUniformLocation;
	}
	
	interface VertexAttributesDictionary
	{
		[name: string]: number;
	}


	export class Shader
	{
		static attributeNames = {
			/**
			 * Vertex attributes
			 */
			vertexAttributes : {
				position : "position",
				color    : "color",
				uv1      : "uv1"
			},
			
			/**
			 * Uniform attributes
			 */
			uniforms : {
				cameraMatrix : "cameraMatrix",
				sampler1     : "sampler1"
			}
		}
		
		private vertexShaderSource:string
		private fragmentShaderSrouce:string
		private gl:WebGLRenderingContext
		
		private vShader:WebGLShader
		private fShader:WebGLShader
		
		private program:WebGLProgram
		
		uniforms:UniformsDictionary = {}
		vertexAttributes:VertexAttributesDictionary = {}
		
		constructor(glContext:WebGLRenderingContext, vertexShaderSource:string, fragmentShaderSource:string)
		{
			this.gl = glContext
			
			this.vertexShaderSource = vertexShaderSource
			this.fragmentShaderSrouce = fragmentShaderSource	
			this.compileShaders()
			this.linkShaders()	
		}
		
		
		/**
		 * ტვირთავს {WebGLProgram}-ს რომელიც აწერილი იყო DOM-ის ელემენტში იდენტიფიკატორით htmlElementID
		 * 
		 * 
		 */
		static getShaderSourceFromDOM( htmlElementID:string ):string {
			
			var shaderScript:HTMLScriptElement
			var theSource:string
			var currentChild:Node
			
			shaderScript = <HTMLScriptElement>document.getElementById( htmlElementID )
			
			if (!shaderScript) {
				return null
			}
			
			theSource = ""
			currentChild = shaderScript.firstChild
			
			//მოვიპოვებ შეიდერის სორსს
			while (currentChild) {
				
				if (currentChild.nodeType == currentChild.TEXT_NODE) {
					
					theSource += currentChild.textContent
					
				}
				
				currentChild = currentChild.nextSibling
				
			}
			
			return theSource
					
		}	
		
		addVertexAttribute(attributeName:string):void
		{
			var vertexAttributeArray = this.gl.getAttribLocation( this.program, attributeName )
			this.gl.enableVertexAttribArray( vertexAttributeArray )
			
			this.vertexAttributes[attributeName] = vertexAttributeArray
		}
		
		addUniform(uniformName:string):void
		{
			this.uniforms[uniformName] = this.gl.getUniformLocation(this.program, uniformName)
		}
		
		getUniform(name:string):WebGLUniformLocation
		{
			return this.uniforms[name]
		}
		
		getVertexAttribute(name:string):number
		{
			return this.vertexAttributes[name]
		}
		
		private compileShaders()
		{
			
			this.fShader = this.gl.createShader(this.gl.FRAGMENT_SHADER)
			this.gl.shaderSource(this.fShader, this.fragmentShaderSrouce)
			this.gl.compileShader(this.fShader)
			
			this.vShader = this.gl.createShader(this.gl.VERTEX_SHADER)
			this.gl.shaderSource(this.vShader, this.vertexShaderSource)
			this.gl.compileShader(this.vShader)
			
			//ვამოწმებ წარმატებით დაკომპილირდა თუ არა შეიდერი
			if (!this.gl.getShaderParameter(this.vShader, this.gl.COMPILE_STATUS)) {  
				console.log("An error occurred compiling the shaders: " + this.gl.getShaderInfoLog(this.vShader));  
				return null;  
			}	
			
			//ვამოწმებ წარმატებით დაკომპილირდა თუ არა შეიდერი
			if (!this.gl.getShaderParameter(this.fShader, this.gl.COMPILE_STATUS)) {  
				console.log("An error occurred compiling the shaders: " + this.gl.getShaderInfoLog(this.fShader));  
				return null;  
			}	
											
		}
		
		private linkShaders()
		{
			this.program = this.gl.createProgram()
			
			this.gl.attachShader(this.program, this.vShader)
			this.gl.attachShader(this.program, this.fShader)
			
			this.gl.linkProgram(this.program)
			
			if ( !this.gl.getProgramParameter( this.program, this.gl.LINK_STATUS ) ) {
				console.log("Unable to initialize the shader program.")
			}
		}
		
		useShaderProgram()
		{
			this.gl.useProgram(this.program)
		}
		
		setSampler2D(uniformName:string, texture2d:Texture2D | TextureBackedFramebuffer):void
		{
			if(texture2d instanceof Texture2D)
			{
				texture2d.useTexture(this.gl.TEXTURE0)
				this.gl.uniform1i(this.uniforms[uniformName], 0)	
			}
			
			if(texture2d instanceof TextureBackedFramebuffer)
			{
				texture2d.useRenderTexture(this.gl.TEXTURE1)
				this.gl.uniform1i(this.uniforms[uniformName], 1)
				console.log("render texture")
			}			
			
			
		}
		
	}
	
}