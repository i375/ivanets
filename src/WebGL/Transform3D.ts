/// <reference path="../../IvaneMain.ts" />

module Ivane.WebGL
{
	export class Transform3D
	{
		transformMat4:GLM.IArray
		
		private translate3f:GLM.IArray
		private scale3f:GLM.IArray
		private rotation3f:GLM.IArray
		
		constructor()
		{
			
			this.transformMat4 = mat4.create()
			mat4.identity(this.transformMat4)
			
			this.translate3f = vec3.create()
			
			this.scale3f = vec3.create()
			vec3.set(this.scale3f, 1.0, 1.0, 1.0)
			
			this.rotation3f = vec3.create()
			vec3.set(this.rotation3f, 0.0, 0.0, 0.0)
			
			this.computeTransformMatrix()
			
		}
        
        getScale3f():GLM.IArray
        {
            return this.scale3f
        }
        
        getTranslation3f():GLM.IArray
        {
            return this.translate3f
        }
        
        getRotation3f():GLM.IArray
        {
            return this.rotation3f
        }
		
		scale(x:number, y:number, z:number)
		{
			vec3.set(this.scale3f, x,y,z)
			
			this.computeTransformMatrix()
		}
		
		translate(x:number, y:number, z:number)
		{
			vec3.set(this.translate3f, x, y, z)
			
			this.computeTransformMatrix()
		}
		
		rotate(x:number, y:number, z:number)
		{
			vec3.set(this.rotation3f, x, y, z)
			
			this.computeTransformMatrix()
		}
		
		private _scale()
		{
			mat4.scale(this.transformMat4, this.transformMat4, this.scale3f)
		}
		
		private _translate()
		{
			mat4.translate(this.transformMat4, this.transformMat4, this.translate3f)
		}
		
		private _rotationAxis3f = vec3.create()
		
		private _rotate()
		{
			//ვატრიალებ x ღერძის გარშემო
			vec3.set(this._rotationAxis3f, 1.0, 0.0, 0.0)
			mat4.rotate(this.transformMat4, this.transformMat4, this.rotation3f[0], this._rotationAxis3f)
			
			//ვატრიალებ y ღერძის გარშემო
			vec3.set(this._rotationAxis3f, 0.0, 1.0, 0.0)
			mat4.rotate(this.transformMat4, this.transformMat4, this.rotation3f[1], this._rotationAxis3f)
			
			//ვატრიალებ z ღერძის გარშემო
			vec3.set(this._rotationAxis3f, 0.0, 0.0, 1.0)
			mat4.rotate(this.transformMat4, this.transformMat4, this.rotation3f[2], this._rotationAxis3f)						
		}
		
		private _setTransformMatrixToIdentity()
		{
			mat4.identity(this.transformMat4)
		}
		
		private computeTransformMatrix()
		{
			this._setTransformMatrixToIdentity()
			this._scale()
			this._translate()	
			this._rotate()
			
		}
			
	}
	
}