/// <reference path="../../IvaneMain.ts" />

module Ivane.WebGL
{
	
	export class Texture2D
	{
		
		private gl:WebGLRenderingContext
		private textureImage:HTMLImageElement
		private textureImageSource:string
		private texture:WebGLTexture
		
		constructor(glContext:WebGLRenderingContext, textureImageSource:string)
		{
			
			this.gl = glContext
			
			//ვქმნი ტექსტურის GL ობიექტს
			this.texture = this.gl.createTexture()
			
			this.textureImageSource = textureImageSource
			
			this.loadTextureImage()
			
		}
        
		private loadTextureImage()
		{
			this.textureImage = new Image()
			
			this.textureImage.onload = () => {
				this.handleTextureImageLoaded()
			}	
			
			this.textureImage.src = this.textureImageSource
		
		}
		
		private handleTextureImageLoaded()
		{
			
			//ვააქტიურებს ტექსტურის ობიექტს
			this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture)
			//ვთვირთავს ტექსტურის მონაცემებს, რეალურ ანუ რეალურ სურათს
			this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, this.textureImage)
			
			//ვსაზღვრავს გადიდების დროს გამოიყენოს წრფივი ფილტრი
			this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR)
			//ვსაზღვრავ რომ ტექსტურის შემცირებისას გამოიყენოს MIPMAP ტექსტურა და მასზე წრფივი ფილტრი
			this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR_MIPMAP_NEAREST)
			
			//გააქტიურებული ტექსტურის ობიექტსიათვის რომელშიც სურათი უკვე ჩატვირტულია
			//ვაგენერირებ MIPMAP ტექსტურებს
			this.gl.generateMipmap(this.gl.TEXTURE_2D)
			
			//მოვემულ ტექსტურის ბოეიქტს ვხსნი აქტიური ობიექტის სტატუსიდან
			//ანუ მოვრჩი ტექსტურის რედაქტირებას
			this.gl.bindTexture(this.gl.TEXTURE_2D, null)			
			
		}
		
		/**
		 * 
		 * ააქტიურებს ტექსტურას, მითითებულ GL texture unit-ში
		 * gl.TEXTURE0, gl.TEXTURE1 ...
		 * 
		 */
		useTexture(textureUnit?:number)
		{
			
			var texUnit = this.gl.TEXTURE0
			
			if(textureUnit)
			{
				texUnit = textureUnit
			}
			
			//ვააქტიურებს ტექსტურის სლოტს
			this.gl.activeTexture(texUnit)
			this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture)
			
		}
		
		
	}
	
}