/// <reference path="../../IvaneMain.ts" />

module Ivane.WebGL
{
    
    export enum VertexAttributeElementTypes
    {
        Float32,
        Uint16
    }
    
    export class VertexAttributeArray
    {
        buffer: Float32Array|Uint16Array
        elementsPerAttribute: number 
        elementType: VertexAttributeElementTypes
        
        constructor(elementsCount:number, elementsPerAttribute:number, elementType:VertexAttributeElementTypes)
        {
            
            this.elementsPerAttribute = elementsPerAttribute
            
            if (elementType == VertexAttributeElementTypes.Float32)
            {
                this.buffer = new Float32Array(elementsCount * this.elementsPerAttribute)
            }
            else if (elementType == VertexAttributeElementTypes.Uint16)
            {
                throw new Ivane.Exceptions.NotImplemetedException()
            }
            
        }
        
		/**
		 * თვლის რამდენი წვერო შეიძლება გათავსდეს ბუფერში
		 */
		getElementsCount():number
		{
            
			return this.buffer.length / this.elementsPerAttribute
            
		}
        
		checkIfVertexNumberIsWithinBounds(vertexNumber:number)
		{
			const maxVertexNumber = this.getElementsCount() - 1
			
			if(vertexNumber > maxVertexNumber)
			{
				throw Ivane.Exceptions.VertexNumberOutOfVerexBuffer()
			}
			
		}  
        
		/**
		 * ბუფერში წერს შესაბამისი ნომრის მქონე წვეროს ატრიბუტის ელემენტს.
		 */
		setElement2f(vertexNumber:number, x:number, y:number)
		{
            
			this.checkIfVertexNumberIsWithinBounds(vertexNumber)
			
			//წვეროს რეალური ინდექსი წვეროების ერთიან ბუფერში
			var elementIndex = vertexNumber * this.elementsPerAttribute
			
			this.buffer[elementIndex]     = x
			this.buffer[elementIndex + 1] = y
            
		}	        
        
		/**
		 * ბუფერში წერს შესაბამისი ნომრის მქონე წვეროს ატრიბუტის ელემენტს.
		 */
		setElement3f(vertexNumber:number, x:number, y:number, z:number)
		{
			this.checkIfVertexNumberIsWithinBounds(vertexNumber)
			
			//წვეროს რეალური ინდექსი წვეროების ერთიან ბუფერში
			var elementIndex = vertexNumber * this.elementsPerAttribute
			
			this.buffer[elementIndex]     = x
			this.buffer[elementIndex + 1] = y
            this.buffer[elementIndex + 2] = z
            
		}	              

		/**
		 * ბუფერში წერს შესაბამისი ნომრის მქონე წვეროს ატრიბუტის ელემენტს.
		 */
		setElement4f(vertexNumber:number, x:number, y:number, z:number, a:number)
		{
            
			this.checkIfVertexNumberIsWithinBounds(vertexNumber)
			
			//წვეროს რეალური ინდექსი წვეროების ერთიან ბუფერში
			var elementIndex = vertexNumber * this.elementsPerAttribute
			
			this.buffer[elementIndex]     = x
			this.buffer[elementIndex + 1] = y
			this.buffer[elementIndex + 2] = z
			this.buffer[elementIndex + 3] = a
            
		}	
        
		/**
		 * აკოპირებს მითითებული ნომრის მქონე ელემენტის კომპონენტებს.
		 */
		getElement2f(out:GLM.IArray, elementNumber:number):void
		{
			
			//წვეროს რეალური ინდექსი წვეროების ერთიან ბუფერში
			var elementIndex = elementNumber * this.elementsPerAttribute
			
			out[0] = this.buffer[elementIndex]
			out[1] = this.buffer[elementIndex + 1]
				
		}        
             
		/**
		 * აკოპირებს მითითებული ნომრის მქონე ელემენტის კომპონენტებს.
		 */
		getElement4f(out:GLM.IArray, elementNumber:number):void
		{
			
			//წვეროს რეალური ინდექსი წვეროების ერთიან ბუფერში
			var elementIndex = elementNumber * this.elementsPerAttribute
			
			out[0] = this.buffer[elementIndex]
			out[1] = this.buffer[elementIndex + 1]
			out[2] = this.buffer[elementIndex + 2]
			out[3] = this.buffer[elementIndex + 3]
				
		}      
        
		/**
		 * აკოპირებს მითითებული ნომრის მქონე ელემენტის კომპონენტებს.
		 */
		getElement3f(out:GLM.IArray, elementNumber:number):void
		{
			
			//წვეროს რეალური ინდექსი წვეროების ერთიან ბუფერში
			var elementIndex = elementNumber * this.elementsPerAttribute
			
			out[0] = this.buffer[elementIndex]
			out[1] = this.buffer[elementIndex + 1]
            out[2] = this.buffer[elementIndex + 2]
				
		}                        
                
    }   
    
    export function createVertexUVArray2f(vertexCount:number):VertexAttributeArray
    {
        return new VertexAttributeArray(vertexCount, 2, VertexAttributeElementTypes.Float32)
    }
    
    export function createVertexColorArray4f(vertexCount:number):VertexAttributeArray
    {
        return new VertexAttributeArray(vertexCount, 4, VertexAttributeElementTypes.Float32)
    }

    export function createVertexPositionArray3f(vertexCount:number):VertexAttributeArray
    {
        return new VertexAttributeArray(vertexCount, 3, VertexAttributeElementTypes.Float32) 
    }    
          
}