/*
 * Author Ivane Gegia http://ivane.info
 */

///<reference path="DeltaTime.ts"/>
///<reference path="CanvasInputsManager.ts"/>
/// <reference path="../definitions/gl-matrix/gl-matrix.d.ts" />


module Ivane.GameClasses
{
	
	export class GameClassWebGL
	{
		canvasElement:HTMLCanvasElement
		gl:WebGLRenderingContext
		
		deltaTime:number
		inputsManager:Ivane.Inputs.CanvasInputsManager	
		
		viewRatio:number = 0.0
		private deltaTimeComputer:Ivane.Time.DeltaTimeComputer
		
		private canvasCenterX:number
		private canvasCenterY:number	
				
		constructor(width:number, height:number, canvasContainer:HTMLElement, preserveDrawingBuffer:boolean)
		{
			
			this.canvasElement = <HTMLCanvasElement>document.createElement("canvas")
			this.canvasElement.width = width
			this.canvasElement.height = height	
			
			canvasContainer.appendChild(this.canvasElement)
			
			//webgl კონტექსტის მოპოვება
			this.gl = <WebGLRenderingContext>this.canvasElement.getContext("webgl", {
										preserveDrawingBuffer: preserveDrawingBuffer
			})
			
			//დროის delta-ს მთველელი ინიციალიზაცია
			this.deltaTimeComputer = new Ivane.Time.DeltaTimeComputer()
			this.deltaTime = this.deltaTimeComputer.getDeltaTimeInSeconds()
			
			//canvasElement-ზე inputsManager-ირ ინიციალიზაცია რომ ააგროვოს input-ი
			this.inputsManager = new Ivane.Inputs.CanvasInputsManager()
			this.inputsManager.startProcessingInputFor(this.canvasElement)		
			
			this.updateViewRatioAndCanvasCenterXY(width, height)	
			
		}	
		
		updateViewRatioAndCanvasCenterXY(width:number, height:number):void
		{
			this.viewRatio = width / height
			
			//canvas ელემენტის ცენტრის კოორდინატები pixel-ებში
			this.canvasCenterX = this.canvasElement.width / 2
			this.canvasCenterY = this.canvasElement.height / 2			
		}
		
		/**
		 * ინახავს მაუსის პიქსელურ კოორდინატებს 
		 * გადაყვანილს webgl-ის ხედის კოორდინატებში
		 * 
		 * (-1, 1)----------(1,1)
		 * |          |        |
		 * |          |        |
		 * |          |        |
		 * |---------(0,0)-----|
		 * |          |        |
		 * |          |        |
		 * |          |        |
		 * (-1,-1)---------(1,-1)
		 */
		mouseViewCoordiantes:GLM.IArray = vec3.create()	
		
		/**
		 * მაუსის ხედის კოორდინატები, სიგანე-სიმაღლის შეფარდების გათვალისწინებით.
		 * ანუ მართკუთხა ხედის შემთხვევისთვის
		 * თუ მაგალითად ხედი არის განიერი, ამ შემთხვევაში x კოორდინატი გაცდება [-1,1] სეგმენტს
		 * 
		 * (-1, 1)----------(1,1)
		 * |          |        |
		 * |          |        |  *(კოორდინატი, widescreen შემთხვევაში)
		 * |          |        |
		 * |---------(0,0)-----|
		 * |          |        |
		 * |          |        |
		 * |          |        |
		 * (-1,-1)---------(1,-1)
		 */
		mouseViewCoordiantesProportional:GLM.IArray = vec3.create()	
	
		//მუასის პიქსელი კოორდინატების დროებითი შემნახველი ვექტორი	
		mousePixelCoordinatesCentered:GLM.IArray = vec3.create()
		
		/**
		 * გადაყავს მაუსის ხედის პიქსეული კოორდინატები, 
		 * webgl_ის ხედის კოორდინატებში
		 */
		private computeMouseViewCoordinates()
		{
			//ვაკოპირებ მაუსის პიქსეულ კოორდინატებს
			vec2.set(this.mousePixelCoordinatesCentered, this.inputsManager.mouseXY.x, this.inputsManager.mouseXY.y)
			
			//ვაკონვერტირებ კოორდინატებს ისე თითქოს ცენტრი ითველობდეს ხედის ცენტრიდან
			this.mousePixelCoordinatesCentered[0] = this.mousePixelCoordinatesCentered[0] - this.canvasCenterX
			this.mousePixelCoordinatesCentered[1] = this.mousePixelCoordinatesCentered[1] - this.canvasCenterY
			
			//ვატრიალებ y კოორდინატს
			this.mousePixelCoordinatesCentered[1] = -1.0 * this.mousePixelCoordinatesCentered[1]	
			
			//პროპორციული კოორდინატების დათვლა, ანუ აქ ვითვალისწინებ სიგანე-სიმაღლის შეფარდებას
			this.mouseViewCoordiantesProportional[0] = this.mousePixelCoordinatesCentered[0] / this.canvasCenterX
			this.mouseViewCoordiantesProportional[1] = this.mousePixelCoordinatesCentered[1] / this.canvasCenterY
			this.mouseViewCoordiantesProportional[0] = this.mouseViewCoordiantesProportional[0] * this.viewRatio			
						
			//კოორდინატების გადაყვანა ერთეულოვან სივრცეში
			this.mouseViewCoordiantes[0] = this.mousePixelCoordinatesCentered[0] / this.canvasCenterX
			this.mouseViewCoordiantes[1] = this.mousePixelCoordinatesCentered[1] / this.canvasCenterY						
		}	
		
		/**
		 * ფუნცია რომელის სტარტავს game loop-ს
		 */
		run():void
		{			
			this.animationFrameFunction()
		}	
		
		private animationFrameFunction()
		{
			this.deltaTime = this.deltaTimeComputer.getDeltaTimeInSeconds()
				
			this.inputsManager.processInput()
			this.computeMouseViewCoordinates()

			this.gameStep()
			
			this.inputsManager.postProcessInput()
			
			requestAnimationFrame(()=>{
				this.animationFrameFunction()
			})
			
		}	
		
		gameStep():void
		{
			throw new Ivane.Exceptions.NotImplemetedException()
		}			
		
		
	}
	
}