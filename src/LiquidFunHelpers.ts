/*
 * Author Ivane Gegia https://twitter.com/IvaneGegia
 */

/// <reference path="../definitions/liquidfun/liquidfun.d.ts" />
/// <reference path="Exceptions.ts" />
/// <reference path="Assertion.ts" />

module Ivane.LiquidFunHelpers
{
	/**
	 * ქმნის ფიზიკურ სამყაროს და არეგისტრირებს როგორც გლობალური ცვლადი JavaScript სივრცეში.
	 * ეს ხდება მხოლოდ LiquidFun-ის JavaScript-ის ვერსიის სპეციფიკის გამო.
	 * 
	 * @param gravity გრავიტაციის ვექტორი
	 */
	export function createWorldAndRegisterItAsGlobalVariable(gravity:b2Vec2):b2World
	{
		var world = new b2World(gravity)
		window["world"] = world
		
		return world;
	}
	
	export function getWorld()
	{
		return window["world"]
	}
	
	/**
	 * 
	 * მსუბუქი უტილიტა ფუნქცია რომელიც ქმნის ნაწილაკების სისტემას.
	 * 
	 * @param world_ref ფიზიკურ სამყაროზე მიმთითებელი, რომელშიც უნდა შეიქმნას ნაწილაკების სისტემა
	 * @param particlesRadius ნაწილაკების რადიუსი
	 * @param particleSystemDencity ნაწილაკების სისტემის სიმკვრივის კოეფიციენტი
	 * 
	 */
	export function createParticleSystem(world_ref:b2World, particlesRadius:number, particleSystemDencity:number) : b2ParticleSystem {
		
		var particleSystemDef = new b2ParticleSystemDef()
		var particleSystem = world_ref.CreateParticleSystem(particleSystemDef)
		
		particleSystem.SetRadius( particlesRadius )
		particleSystem.SetDensity( particleSystemDencity )
		
		return particleSystem
		
	}
	
	/**
	 * წაანაცლებს მითითეუბლი ოდენობით shape-ს vertex-ებს
	 * 
	 */
	export function offsetVertexPositions(shape:b2PolygonShape, offset:GLM.IArray)
	{
		for(var vertexIndex = 0; vertexIndex < shape.vertices.length; vertexIndex++)
		{
			var vertex = shape.vertices[vertexIndex]
		
			shape.vertices[vertexIndex].Set( vertex.x + offset[0], vertex.y + offset[1] )
		}	
	}
	
	/**
	 * 
	 * ქმნის სითხის ნაწილაკების, წრის ფორმის, ჯგუფს.
	 * 
	 * @param particleSystem_ref მიმთითებელი ნაწილაკების სისტემაზე, რომელშიც უნდა შეიქმნას ჯგუფი
	 * @param groupRadius ჯგუფის ცრიული ფორმის რადიუსი
	 * @param groupPosition ჯგუფის პოზიცია სამყაროში
	 * 
	 */
	export function createParticleGroupCircular( particleSystem_ref : b2ParticleSystem, groupRadius:number, groupPosition:b2Vec2 ) : b2ParticleGroup {
		
		var particleGroupDef = new b2ParticleGroupDef()
		
		var circleShape = new b2CircleShape()
		circleShape.radius = groupRadius
		
		particleGroupDef.shape = circleShape
		particleGroupDef.angle = 0
		particleGroupDef.position = groupPosition
		particleGroupDef.flags = b2_waterParticle
		
		var particleGroup = particleSystem_ref.CreateParticleGroup( particleGroupDef ) // TODO: რეალური პარამეტრი გადავცე
				
		return particleGroup
		
	}
	
	/**
	 * ქმნის კავშირს ორ ობიექტს შორის, სადაც A და B ობიექტებზე ფიქსირდება წერტილები რომლებიც ერთად უნდა იყონ და ამ წერტილის გარშემო
	 * A და B ობიექტებს შეუძლიათ ბრუნვა.
	 * 
	 * @param world_ref ფიზიკური სამყარო
	 * @param bodyA ობიექტი A
	 * @param bodyB ობიექტი B
	 * @param sharedAnchorInWorldSpace წერტილი სივრცეში რომლის გარშემოც შეუძლიათ იტრიალონ A და B ობიექტებმა, ეს არის ასევე მათი საყრდენი შემაერთები წერტილი.
	 * 
	 */
	export function createRevoluteJoint(
		world_ref:b2World,
		bodyA:b2Body,
		bodyB:b2Body,
		sharedAnchorInWorldSpace:b2Vec2
	) : b2RevoluteJoint
	{
		var revoluteJointDef = new b2RevoluteJointDef()
		
		revoluteJointDef.InitializeAndCreate
		(
			bodyA,
			bodyB,
			sharedAnchorInWorldSpace	
		)	
		
		revoluteJointDef.localAnchorA = bodyA.GetLocalPoint(sharedAnchorInWorldSpace)
		
		revoluteJointDef.localAnchorB = bodyB.GetLocalPoint(sharedAnchorInWorldSpace)
				
		var revoluteJoint = <b2RevoluteJoint>world_ref.CreateJoint(revoluteJointDef)
			
		return revoluteJoint
	}
	
	/**
	 * ქმნის მანძილის შემზღუდველს კავშირს ორ ობიექტს შორის.
	 * 
	 * @param world_ref ფიზიკური სამყარო
	 * @param bodyA პირველი ობიექტი
	 * @param bodyB მეორე ობიექტი
	 * @param anchorA_inLocalSpace საყრდენი წერტილი პირველი ობიექტისთვის, მისსავე ლოკალურ საკოორდინატო სივრცეში
	 * @param anchorB_inLocalSpace საყრდენი წერტილი მეორე ობიექტისათვის, მისსავე ლოკალურ საკოორდინატო სივრცეში
	 * @param dampingRatio სიმკვრივის კოეფიციენტი, რეკომენდებულია 1.0
	 * @param frequencyHz სიმკვრივის კორექტირების სიხშირე, რეკომენდებულია 4.0
	 */
	export function createDistanceJoint(
		world_ref:b2World,
		bodyA:b2Body,
		bodyB:b2Body,
		anchorA_inLocalSpace:b2Vec2,
		anchorB_inLocalSpace:b2Vec2,
		dampingRatio:number,//1 is recomended
		frequencyHz:number//4 is recomended
	) : b2DistanceJoint
	{		
		var distanceJointDef = new b2DistanceJointDef()

		//Calculating b2DistanceJointDef::length
		var anchorAWorldPosition = new b2Vec2(
			bodyA.GetPosition().x + anchorA_inLocalSpace.x,
			bodyA.GetPosition().y + anchorA_inLocalSpace.y)

		var anchorBWorldPosition = new b2Vec2(
			bodyB.GetPosition().x + anchorB_inLocalSpace.x,
			bodyB.GetPosition().y + anchorB_inLocalSpace.y
			)

		var distanceBetweenAnchorAAndAnchorB = Math.sqrt(
			Math.pow(
				Math.abs(anchorBWorldPosition.x - anchorAWorldPosition.x),
				2)
			+ Math.pow(
				Math.abs(anchorBWorldPosition.y - anchorAWorldPosition.y),
				2)
			)
						
		Ivane.Assertion.DynamicAssert(distanceBetweenAnchorAAndAnchorB > .1, "value: " 
			+ distanceBetweenAnchorAAndAnchorB.toString() + 
			" for b2DitanceJoint::length is too small")			
		
		distanceJointDef.length = distanceBetweenAnchorAAndAnchorB
		
		distanceJointDef.dampingRatio = dampingRatio
		distanceJointDef.frequencyHz = frequencyHz
		
		distanceJointDef.InitializeAndCreate(bodyA,bodyB,anchorA_inLocalSpace,anchorB_inLocalSpace)
		
		distanceJointDef.localAnchorA.Set(anchorA_inLocalSpace.x,anchorA_inLocalSpace.y)
		distanceJointDef.localAnchorB.Set(anchorB_inLocalSpace.x,anchorB_inLocalSpace.y)
		
		var distanceJoint = <b2DistanceJoint>world_ref.CreateJoint(distanceJointDef)
		
		return distanceJoint
	}
		
	/**
	 * ქმნის სხეულს რომელზეც მოქმედებს ფიზიკური ენერგიები და მათი გავლენით გადაადგილდება.
	 */
	export function createDynamicBody (
		world_ref:b2World,
		shapes:b2Shape[], 
		density:number,
		friction:number,
		position:b2Vec2,
		linearDamping:number,
		angularDamping:number,
		fixedRotation:boolean,
		bullet:boolean,
		restitution:number,
		userData:number,
		filter_nullable:b2Filter
	) : b2Body
	{
		
		var bodyDefinition = new b2BodyDef()
		bodyDefinition.active = true
		bodyDefinition.position = position
		bodyDefinition.angularDamping = angularDamping
		bodyDefinition.linearDamping = linearDamping
		bodyDefinition.bullet = bullet
		bodyDefinition.type = b2_dynamicBody
		bodyDefinition.userData = userData
		bodyDefinition.filter = new b2Filter()
		
		var dynamicBody = world_ref.CreateBody(bodyDefinition)		

		for (var shapeIndex = 0; shapeIndex < shapes.length; shapeIndex++) 
		{
				
			var bodyFixtureDefinition = new b2FixtureDef()
			bodyFixtureDefinition.density = density
			bodyFixtureDefinition.friction = friction
			bodyFixtureDefinition.shape = shapes[shapeIndex]
			bodyFixtureDefinition.restitution = restitution
			
			if(filter_nullable != null)
			{
				bodyFixtureDefinition.filter = filter_nullable
			}
			
			dynamicBody.CreateFixtureFromDef(bodyFixtureDefinition)
			
		}
		
		return dynamicBody
		
	}
	
	/**
	 * ქმნის სხეულს რომელზეც არ მოქმედებს გრავიტაცია დას სხვა ფიზიკური ენერგიები მაგრამ დასაშვებია ჰქონდეს სიჩქარე.
	 * 
	 */
	export function createKinematicBody (
		world_ref:b2World,
		shape:b2Shape, 
		friction:number,
		position:b2Vec2,
		linearDamping:number,
		angularDamping:number,
		fixedRotation:boolean,
		bullet:boolean,
		restitution:number,
		userData:number		
	) : b2Body
	{
		var bodyDefinition = new b2BodyDef()
		bodyDefinition.active = true
		bodyDefinition.position = position
		bodyDefinition.angularDamping = angularDamping
		bodyDefinition.linearDamping
		bodyDefinition.bullet = bullet
		bodyDefinition.type = b2_kinematicBody
		bodyDefinition.userData = userData
		
		var kinematicBody = world_ref.CreateBody(bodyDefinition)		
			
		var bodyFixtureDefinition = new b2FixtureDef()
		bodyFixtureDefinition.friction = friction
		bodyFixtureDefinition.shape = shape
		bodyFixtureDefinition.restitution = restitution
		
		kinematicBody.CreateFixtureFromDef(bodyFixtureDefinition)
		
		return kinematicBody		
	}
	
	/**
	 * ქმნის სხეულს რომელიც არ არის გათვლილი მოძრაობაზე
	 */
	export function createStaticBody (
		world_ref:b2World,
		shape:b2Shape, 
		friction:number,
		position:b2Vec2,
		restitution:number,
		userData:number	
	) : b2Body
	{
		var bodyDefinition = new b2BodyDef()
		bodyDefinition.active = true
		bodyDefinition.position = position
		bodyDefinition.type = b2_staticBody
		bodyDefinition.userData = userData
		
		var staticBody = world_ref.CreateBody(bodyDefinition)		
			
		var bodyFixtureDefinition = new b2FixtureDef()
		bodyFixtureDefinition.friction = friction
		bodyFixtureDefinition.shape = shape
		bodyFixtureDefinition.restitution = restitution
		
		staticBody.CreateFixtureFromDef(bodyFixtureDefinition)
		
		return staticBody		
	}
    
    /**
     * ქმნის ფიზიკურ ობიეტს რომლის ფორმაც ისაზღვრება b2ChainShape-თი.
     * 
     * @param world_ref ფიზიკის სამყაროზე მაჩვენებელი
     * @param chainPointsInLocalSpace ჯაშწვის წერტილები, გასაზღვრული ლოკალურ კოორდინატებში
     * @param bodyPosition ფიზიკური ობიექტის პოზიცია ფიზიკურ სამყაროში
     */
    export function createChainShapeBody(
        world_ref:b2World,
        chainPointsInLocalSpace:Array<b2Vec2>,
        bodyPosition?:b2Vec2
    ):b2Body
    {
        var chainShapeBody = null
        
        var chainShape = new b2ChainShape()
        chainShape.vertices = chainPointsInLocalSpace
        

        chainShapeBody = Ivane.LiquidFunHelpers.createStaticBody( world_ref, chainShape, 1, new b2Vec2(0, 0), 0, null )
        
        if ( bodyPosition != undefined )
        {
            chainShapeBody.SetTransform( bodyPosition, 0 )    
        }
        
        return chainShapeBody
    }


}