/*
 * Author Ivane Gegia http://ivane.info
 */
 
module Ivane.Animation {
        export enum EASING_TYPES {
                LINEAR,
                EASE_IN_EASE_OUT
        }

        export interface AnimationStepDelegate {
                (animation: Animation): void;
        }

        /**
         * ანიმაციის აღმწერი კლასი.
         * ამ კლასში ხდება ანიმაციის კოეფიციენტების გადათვლა, შესაბამისი easing-ის ფუქნციების გამოყენებით.
         */
        export class Animation {
                animationStepDelegate: AnimationStepDelegate
                animationFinishedDelegate: AnimationStepDelegate

                easingType: EASING_TYPES

                from: number = 0
                to: number = 0
                active: boolean = false
                durationInSeconds: number

                normalizedProgress: number = 0
                public getNormalizedProgressEasingApplied(): number {
                        switch (this.easingType) {
                                case EASING_TYPES.LINEAR:
                                        return this.normalizedProgress;

                                case EASING_TYPES.EASE_IN_EASE_OUT:
                                        var easingApplied = (Math.sin(this.normalizedProgress * Math.PI - Math.PI / 2) + 1) / 2;
                                        return easingApplied;

                                default:
                                        return 0.0;
                        }
                }

                /**
                 * აბრუნებს მიმდინარე გპროგრესს, მაგალითად თუ ანიმაცია იყო 0-დან 10-მდე, დააბრუნებს 0-დან 10-მდე,
                 * მიმდინარე კადრისათვის შესაბამის მნიშვნელობას.
                 */
                public getProgress(): number {
                        return this.from + (this.to - this.from) * this.getNormalizedProgressEasingApplied();
                }
                
                /**
                 * გამოიყენება {AnimationsManager}-ის მიერ ანიმაციის სამართავად.
                 */
                public stepAnimation(deltaTime: number): void {
                        this.normalizedProgress += deltaTime / this.durationInSeconds;

                        if (this.normalizedProgress > 1.0) {
                                this.normalizedProgress = 1.0;

                                this.animationStepDelegate(this);
                                this.animationFinishedDelegate(this);

                                this.active = false;
                        }
                        else {
                                this.animationStepDelegate(this);
                        }



                }

                /**
                 * ანიმაციის განულება, გამოიყენება {AnimationsManager}-ის მიერ.
                 * მეთოდის არსი მდგომარეობს იმაში რომ {AnimationsManager}-ი ინახავს ანიმაციების რიგს(pool),
                 * რომლიდანაც ხელახლა გამოყენების პრინციპით იღებს ანიმაციების ობიექტებს ანულებს მათ
                 * და ახლად დამატებული მნიშვნელობების ითვლის მისით.
                 */
                public reset
                (
                        from: number, 
                        to: number, 
                        durationInSeconds: number, 
                        easingType: EASING_TYPES, 
                        animationStepDelegate: AnimationStepDelegate, 
                        animationFinishedDelegate: AnimationStepDelegate
                ): void 
                {
                        this.from = from;
                        this.to = to;
                        this.durationInSeconds = durationInSeconds;
                        this.active = true;
                        this.animationStepDelegate = animationStepDelegate;
                        this.animationFinishedDelegate = animationFinishedDelegate;
                        this.normalizedProgress = 0;
                        this.easingType = easingType;
                }
        }

        /**
         * ანიმაციების მენეჯერი.
         * გამოიყენება ანიმაციების რიგის შესაქმნელად და მასში ახალი ანიმაციის ჩასამატებლად.
         * ასევე უზრუნველყოფს ანიამციების განახლება/გადათვლას.
         */
        export class AnimationsManager {
                private animationsPool: Array<Animation>

                /**
                 * @param animationsPoolSize ანიმაციების რიგის ზომა, ანუ რამდენი ანიმაცია შეიძლება მაქსიმუმ ერთდოულად მართოს {AnimationsManager}-მა
                 */
                private createAnimationsPool(animationsPoolSize: number): void {
                        this.animationsPool = new Array<Animation>(animationsPoolSize);

                        for (var animationIndex = 0; animationIndex < animationsPoolSize; animationIndex++) {
                                this.animationsPool[animationIndex] = new Animation();
                        }
                }

                /**
                 * ანიმაციას აყენებს შესასრულებლების რიგში.
                 * 
                 * @param from ანიმაციის საწყისი მნიშვნელობა
                 * @param to ანიმაციის საბოლოო მნიშვნელობა
                 * @param durationInSeconds ანიმაციის ხანგრძლივობა წამებში
                 * @param easingType იზინგის ტიპი
                 * @param animationStepHandler ანიმაციის ბიჯის დამმუშავებელი ფუნქცია, ამ ფუნქციას გადმოიცემა მიმდინარე ანიმაცია, მისი მნიშნველობების გამოსაყენებლად
                 * @param animationFinishedHandler ანიმაციის დასასრულის დამმუშავებელი ფუნქცია, ამ ფუნქციას გადმოიცემა მიმდინარე ანიმაცია, მისი მნიშნველობების გამოსაყენებლად
                 */
                public queueAnimation
                (
                        from: number, 
                        to: number, 
                        durationInSeconds: number, 
                        easingType: EASING_TYPES, 
                        animationStepHandler: AnimationStepDelegate, 
                        animationFinishedHandler: AnimationStepDelegate
                ): void 
                {
                        var animation = this.tryToGetInactiveAnimation();

                        animation.reset(from, to, durationInSeconds, easingType, animationStepHandler, animationFinishedHandler);
                }
                
                public queueFunctionCall(inSeconds:number, func:Function)
                {
                    this.queueAnimation(
                        0.0, 
                        1.0, 
                        inSeconds, 
                        Ivane.Animation.EASING_TYPES.LINEAR,
                        (anim)=>{},
                        (anim)=>{
                            func()
                        })
                }

                /**
                 * ცდის მოიპოვოს თავისუფლაი ანიმაციის ობიექტი, ანიმაციების რიგიდან.
                 */
                private tryToGetInactiveAnimation(): Animation {
                        var animation: Animation = null;

                        for (var animationIndex = 0; animationIndex < this.animationsPool.length; animationIndex++) {
                                animation = this.animationsPool[animationIndex];

                                if (animation.active == false) {
                                        break;
                                }
                        }

                        if (animation.active == true) {
                                animation = null;
                        }

                        return animation;
                }

                /**
                 * აახლებს ანიმაციებს.
                 */
                private stepAnimations(deltaTime: number): void {
                        for (var animationIndex = 0; animationIndex < this.animationsPool.length; animationIndex++) {
                                var animation = this.animationsPool[animationIndex];

                                if (animation.active) {
                                        animation.stepAnimation(deltaTime);
                                }
                        }
                }

                constructor(animationsPoolSize:number) {
                        this.createAnimationsPool(animationsPoolSize);

                }

                /**
                 * ფუნქცია უნდა გმოიძახო gameStep მეთოდში, როცა სამყაროს გადათვლას აკეთებ.
                 * მაგალითად: this.animationsManager1.update(this.deltaTime)
                 * 
                 * @param deltaTime დრო კადრსა და კადრს შორის
                 */
                public update(deltaTime: number): void {
                        this.stepAnimations(deltaTime);
                }
        }
}