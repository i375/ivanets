/*
 * Author Ivane Gegia http://ivane.info
 */


module Ivane.Utils
{
    export interface MessageObserverCallback
    {
        (messageId:string, sender:any):void;
    }
    
    interface MessageObserver
    {
        msgCallbak:MessageObserverCallback
        senderFilter?:any
    }
        
    interface MessageObservers
    {
        [messageId:string]:Array<MessageObserver>
    }
    
    /**
     * საბასო მესიჯების ცვლის მექანიზმი.
     * გამოსადეგია მაშინ როცა ობიექტებმა ერთი მეორეს ვინაობა არ იციან მაგრამ
     * ერთი მეორეს მიერ სათქმელის მოსმენა აინტერესებთ.
     * 
     * მაგალითად: 
     *  საათმა შეიძლება თქვას რომ სკოლაში წასვლის დროა.
     *  ბავშვი გაიგებს ამ მესიჯს, მაგრამ შეიძლება საათის არსებობა არც იცოდეს.
     */
    export class MessageDispatcher
    {
        
        private static defaultDispatcher = null
        
        private msgObservers:MessageObservers = {}
        
        static getDefault():MessageDispatcher
        {
            if(!MessageDispatcher.defaultDispatcher)
            {
                MessageDispatcher.defaultDispatcher = new MessageDispatcher()
            }
            
            return MessageDispatcher.defaultDispatcher
        }
        
        /**
         * ამატების მესიჯის მსმენელს, მსმენელთა სიაშია.
         * 
         * @param messageId მესიჯის იდენტიფიკატორი
         * @param observerCallback ფუნქცია რომელიც გამოიძახება მესიჯის საპასუხოდ
         * @param sender მესიჯის გამომგზავნელი ობიექტი, თუ მაგალითად საჭიროა კონკრეტული რომელიმე ობიექტის მიერ მესიჯების მიღება
         */
        addMessageObserver(messageId:string, observerCallback:MessageObserverCallback, sender?:any):void
        {
            if(!this.msgObservers[messageId])
            {
                this.msgObservers[messageId] = new Array<MessageObserver>()
            }
            
            //ვამატებ მესიჯის დამმუშავებელ ფუნქციას სიაში, ასევე ფილტრს თუ ასეთი გადმოწოდა
            this.msgObservers[messageId].push({
                msgCallbak: observerCallback,
                senderFilter: sender ? sender : null
            })
        }
        
        /**
         * მესიჯის გაგზავნა
         * 
         * @param messageId მესიჯის იდენტიფიკატორი
         * @param sender მესიჯის გამომგზავნელი
         */
        postMessage(messageId:string, sender:any)
        {
            if (this.msgObservers[messageId])
            {
                for(var observerIndex = 0; observerIndex < this.msgObservers[messageId].length; observerIndex++)
                {
                    var msgObserver = this.msgObservers[messageId][observerIndex]
                    
                    if(msgObserver != null && msgObserver.senderFilter != null && msgObserver.senderFilter === sender)
                    {
                        msgObserver.msgCallbak(messageId, sender)    
                    }
                    else if(msgObserver != null && msgObserver.senderFilter == null)
                    {
                        msgObserver.msgCallbak(messageId, sender)
                    }
                    
                }
            }
        }
        
        /**
         * წაშლის მითითებული მესიჯის კონკრეტულ მსმენელს სიიდან
         * 
         * @param messageId მესიჯის იდენტიფიკატორი
         * @param observerCallback მესიჯის მსმენელი
         */
        removeObserver(messageId:number, observerCallback:MessageObserverCallback)
        {
            var msgObservers = this.msgObservers[messageId]
            
            for(var observerIndex = 0; observerIndex < msgObservers.length; observerIndex++)
            {
                if(msgObservers[observerIndex].msgCallbak == observerCallback)
                {
                    msgObservers[observerIndex] = null
                }
            }
            
        }
    }
}