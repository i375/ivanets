/*
 * Author Ivane Gegia http://ivane.info
 */

module Ivane.Utils {

    /**
     * კლასი გამოსადეგია დელეგატის შესაბუთათ, რომელიც gameStep-ში შეიძლება შესრულდეს.
     * და შესრულების შემდეგ გვინდა რომ მიუხედავად გამომძახებელი კოდის კვლავ შესრულებისა
     * შეფუთული ფუნქცია არ შესრულდეს სანამ არ გამოიძახება reset მეთოდი.
     */
    export class FireOnce {
        private callbackCalled = false
        private callback: () => void = null

        constructor(callback: () => void) {
            this.callback = callback
        }

        /**
         * შეასრულოს შეფუთული დელეგატი.
         */
        fire() {
            if (this.callbackCalled === false && this.callback != null) {
                this.callback()
                this.callbackCalled = true
            }
        }

        reset() {
            this.callbackCalled = false
        }
    }

}