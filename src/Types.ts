/// <reference path="../IvaneMain.ts" />

type GLMVec4  = GLM.IArray
type GLMVec3  = GLM.IArray
type GLMVec2  = GLM.IArray
type GLMMat3  = GLM.IArray
type GLMMat4  = GLM.IArray
type GLMMat2  = GLM.IArray
type GLMMat2d = GLM.IArray
type GLMQuat  = GLM.IArray