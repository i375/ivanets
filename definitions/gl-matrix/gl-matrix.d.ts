// Type definitions for gl-matrix 2.2.2
// Project: https://github.com/toji/gl-matrix
// Definitions by: Tat <https://github.com/tatchx>
// Definitions: https://github.com/borisyankov/DefinitelyTyped

/// <reference path="../../src/Types.ts" />


declare module GLM {
    interface IArray
    {
        /**
         * Must be indexable like an array
         */
        [index: number]: number;
    } 
}

// Common
declare module glMatrix {
    /**
    * Convert Degree To Radian
    * 
    * @param a Angle in Degrees
    */  
    export function toRadian(a: number): number;  
}

// vec2
declare module vec2 {
    /**
     * Creates a new, empty vec2
     *
     * @returns a new 2D vector
     */
    export function create(): GLMVec2;
    
    /**
     * Creates a new vec2 initialized with values from an existing vector
     *
     * @param a a vector to clone
     * @returns a new 2D vector
     */
    export function clone(a: GLMVec2): GLMVec2;
    
    /**
     * Creates a new vec2 initialized with the given values
     *
     * @param x X component
     * @param y Y component
     * @returns a new 2D vector
     */
    export function fromValues(x: number, y: number): GLMVec2;
    
    /**
     * Copy the values from one vec2 to another
     *
     * @param out the receiving vector
     * @param a the source vector
     * @returns out
     */
    export function copy(out: GLMVec2, a: GLMVec2): GLMVec2;
    
    /**
     * Set the components of a vec2 to the given values
     *
     * @param out the receiving vector
     * @param x X component
     * @param y Y component
     * @returns out
     */
    export function set(out: GLMVec2, x: number, y: number): GLMVec2;
    
    /**
     * Adds two vec2's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */    
    export function add(out: GLMVec2, a: GLMVec2, b: GLMVec2): GLMVec2;
    
    /**
     * Subtracts vector b from vector a
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */    
    export function subtract(out: GLMVec2, a: GLMVec2, b: GLMVec2): GLMVec2;
    
    /**
     * Subtracts vector b from vector a
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */        
    export function sub(out: GLMVec2, a: GLMVec2, b: GLMVec2): GLMVec2;
    
    /**
     * Multiplies two vec2's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function multiply(out: GLMVec2, a: GLMVec2, b: GLMVec2): GLMVec2;
    
    /**
     * Multiplies two vec2's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */    
    export function mul(out: GLMVec2, a: GLMVec2, b: GLMVec2): GLMVec2;
    
    /**
     * Divides two vec2's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */    
    export function divide(out: GLMVec2, a: GLMVec2, b: GLMVec2): GLMVec2;
    
    /**
     * Divides two vec2's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */        
    export function div(out: GLMVec2, a: GLMVec2, b: GLMVec2): GLMVec2;
    
    /**
     * Returns the minimum of two vec2's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */    
    export function min(out: GLMVec2, a: GLMVec2, b: GLMVec2): GLMVec2;
    
    /**
     * Returns the maximum of two vec2's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */    
    export function max(out: GLMVec2, a: GLMVec2, b: GLMVec2): GLMVec2;
    
    /**
     * Scales a vec2 by a scalar number
     *
     * @param out the receiving vector
     * @param a the vector to scale
     * @param b amount to scale the vector by
     * @returns out
     */    
    export function scale(out: GLMVec2, a: GLMVec2, b: number): GLMVec2;
    
    /**
     * Adds two vec2's after scaling the second operand by a scalar value
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @param scale the amount to scale b by before adding
     * @returns out
     */    
    export function scaleAndAdd(out: GLMVec2, a: GLMVec2, b: GLMVec2, scale: number): GLMVec2;
    
    /**
     * Calculates the euclidian distance between two vec2's
     *
     * @param a the first operand
     * @param b the second operand
     * @returns distance between a and b
     */    
    export function distance(a: GLMVec2, b: GLMVec2): number;
    
    /**
     * Calculates the euclidian distance between two vec2's
     *
     * @param a the first operand
     * @param b the second operand
     * @returns distance between a and b
     */
    export function dist(a: GLMVec2, b: GLMVec2): number;
    
    /**
     * Calculates the squared euclidian distance between two vec2's
     *
     * @param a the first operand
     * @param b the second operand
     * @returns squared distance between a and b
     */
    export function squaredDistance(a: GLMVec2, b: GLMVec2): number;
    
    /**
     * Calculates the squared euclidian distance between two vec2's
     *
     * @param a the first operand
     * @param b the second operand
     * @returns squared distance between a and b
     */
    export function sqrDist(a: GLMVec2, b: GLMVec2): number;
    
    /**
     * Calculates the length of a vec2
     *
     * @param a vector to calculate length of
     * @returns length of a
     */
    export function length(a: GLMVec2): number;
    
    /**
     * Calculates the length of a vec2
     *
     * @param a vector to calculate length of
     * @returns length of a
     */
    export function len(a: GLMVec2): number;
    
    /**
     * Calculates the squared length of a vec2
     *
     * @param a vector to calculate squared length of
     * @returns squared length of a
     */
    export function squaredLength(a: GLMVec2): number;
    
    /**
     * Calculates the squared length of a vec2
     *
     * @param a vector to calculate squared length of
     * @returns squared length of a
     */
    export function sqrLen(a: GLMVec2): number;  
    
    /**
     * Negates the components of a vec2
     *
     * @param out the receiving vector
     * @param a vector to negate
     * @returns out
     */
    export function negate(out: GLMVec2, a: GLMVec2): GLMVec2;
    
    /**
     * Returns the inverse of the components of a vec2
     *
     * @param out the receiving vector
     * @param a vector to invert
     * @returns out
     */
    export function inverse(out: GLMVec2, a: GLMVec2): GLMVec2;
    
    /**
     * Normalize a vec2
     *
     * @param out the receiving vector
     * @param a vector to normalize
     * @returns out
     */
    export function normalize(out: GLMVec2, a: GLMVec2): GLMVec2;
    
    /**
     * Calculates the dot product of two vec2's
     *
     * @param a the first operand
     * @param b the second operand
     * @returns dot product of a and b
     */
    export function dot(a: GLMVec2, b: GLMVec2): number;
    
    /**
     * Computes the cross product of two vec2's
     * Note that the cross product must by definition produce a 3D vector
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function cross(out: GLMVec2, a: GLMVec2, b: GLMVec2): GLMVec2;
    
    /**
     * Performs a linear interpolation between two vec2's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @param t interpolation amount between the two inputs
     * @returns out
     */
    export function lerp(out: GLMVec2, a: GLMVec2, b: GLMVec2, t: number): GLMVec2;
    
    /**
     * Generates a random unit vector
     *
     * @param out the receiving vector
     * @returns out
     */
    export function random(out: GLMVec2): GLMVec2;
    
    /**
     * Generates a random vector with the given scale
     *
     * @param out the receiving vector
     * @param scale Length of the resulting vector. If ommitted, a unit vector will be returned
     * @returns out
     */
    export function random(out: GLMVec2, scale: number): GLMVec2;
    
    /**
     * Transforms the vec2 with a mat2
     *
     * @param out the receiving vector
     * @param a the vector to transform
     * @param m matrix to transform with
     * @returns out
     */
    export function transformMat2(out: GLMVec2, a: GLMVec2, m: GLMMat2): GLMVec2;
    
    /**
     * Transforms the vec2 with a mat2d
     *
     * @param out the receiving vector
     * @param a the vector to transform
     * @param m matrix to transform with
     * @returns out
     */
    export function transformMat2d(out: GLMVec2, a: GLMVec2, m: GLMMat2d): GLMVec2;
    
    /**
     * Transforms the vec2 with a mat3
     * 3rd vector component is implicitly '1'
     *
     * @param out the receiving vector
     * @param a the vector to transform
     * @param m matrix to transform with
     * @returns out
     */
    export function transformMat3(out: GLMVec2, a: GLMVec2, m: GLMMat3): GLMVec2;
    
    /**
     * Transforms the vec2 with a mat4
     * 3rd vector component is implicitly '0'
     * 4th vector component is implicitly '1'
     *
     * @param out the receiving vector
     * @param a the vector to transform
     * @param m matrix to transform with
     * @returns out
     */
    export function transformMat4(out: GLMVec2, a: GLMVec2, m: GLMMat4): GLMVec2;
    
    /**
     * Perform some operation over an array of vec2s.
     *
     * @param a the array of vectors to iterate over
     * @param stride Number of elements between the start of each vec2. If 0 assumes tightly packed
     * @param offset Number of elements to skip at the beginning of the array
     * @param count Number of vec2s to iterate over. If 0 iterates over entire array
     * @param fn Function to call for each vector in the array
     * @param arg additional argument to pass to fn
     * @returns a
     */
    export function forEach(a: GLM.IArray, stride: number, offset: number, count: number,
        fn: (a: GLM.IArray, b: GLM.IArray, arg: any) => void, arg: any): GLM.IArray;
        
    /**
     * Perform some operation over an array of vec2s.
     *
     * @param a the array of vectors to iterate over
     * @param stride Number of elements between the start of each vec2. If 0 assumes tightly packed
     * @param offset Number of elements to skip at the beginning of the array
     * @param count Number of vec2s to iterate over. If 0 iterates over entire array
     * @param fn Function to call for each vector in the array
     * @returns a
     */
    export function forEach(a: GLM.IArray, stride: number, offset: number, count: number,
        fn: (a: GLM.IArray, b: GLM.IArray) => void): GLM.IArray;
        
    /**
     * Returns a string representation of a vector
     *
     * @param vec vector to represent as a string
     * @returns string representation of the vector
     */
    export function str(a: GLM.IArray): string;    
}

// vec3
declare module vec3 {
    
    /**
     * Creates a new, empty vec3
     *
     * @returns a new 3D vector
     */
    export function create(): GLMVec3;
    
    /**
     * Creates a new vec3 initialized with values from an existing vector
     *
     * @param a vector to clone
     * @returns a new 3D vector
     */
    export function clone(a: GLMVec3): GLMVec3;
    
    /**
     * Creates a new vec3 initialized with the given values
     *
     * @param x X component
     * @param y Y component
     * @param z Z component
     * @returns a new 3D vector
     */
    export function fromValues(x: number, y: number, z: number): GLMVec3;
    
    /**
     * Copy the values from one vec3 to another
     *
     * @param out the receiving vector
     * @param a the source vector
     * @returns out
     */
    export function copy(out: GLMVec3, a: GLMVec3): GLMVec3;
    
    /**
     * Set the components of a vec3 to the given values
     *
     * @param out the receiving vector
     * @param x X component
     * @param y Y component
     * @param z Z component
     * @returns out
     */
    export function set(out: GLMVec3, x: number, y: number, z: number): GLMVec3;
    
    /**
     * Adds two vec3's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function add(out: GLMVec3, a: GLMVec3, b: GLMVec3): GLMVec3;
    
    /**
     * Subtracts vector b from vector a
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function subtract(out: GLMVec3, a: GLMVec3, b: GLMVec3):GLMVec3;
    
    /**
     * Subtracts vector b from vector a
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function sub(out: GLMVec3, a: GLMVec3, b: GLMVec3): GLMVec3
    
    /**
     * Multiplies two vec3's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function multiply(out: GLMVec3, a: GLMVec3, b: GLMVec3): GLMVec3;
    
    /**
     * Multiplies two vec3's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function mul(out: GLMVec3, a: GLMVec3, b: GLMVec3): GLMVec3;
    
    /**
     * Divides two vec3's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function divide(out: GLMVec3, a: GLMVec3, b: GLMVec3): GLMVec3;
    
    /**
     * Divides two vec3's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function div(out: GLMVec3, a: GLMVec3, b: GLMVec3): GLMVec3;
    
    /**
     * Returns the minimum of two vec3's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function min(out: GLMVec3, a: GLMVec3, b: GLMVec3): GLMVec3;
    
    /**
     * Returns the maximum of two vec3's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function max(out: GLMVec3, a: GLMVec3, b: GLMVec3): GLMVec3;  
    
    /**
     * Scales a vec3 by a scalar number
     *
     * @param out the receiving vector
     * @param a the vector to scale
     * @param b amount to scale the vector by
     * @returns out
     */
    export function scale(out: GLMVec3, a: GLMVec3, b: number): GLMVec3;
    
    /**
     * Adds two vec3's after scaling the second operand by a scalar value
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @param scale the amount to scale b by before adding
     * @returns out
     */
    export function scaleAndAdd(out: GLMVec3, a: GLMVec3, b: GLMVec3, scale: number): GLMVec3;
    
    /**
     * Calculates the euclidian distance between two vec3's
     *
     * @param a the first operand
     * @param b the second operand
     * @returns distance between a and b
     */
    export function distance(a: GLMVec3, b: GLMVec3): number;
    
    /**
     * Calculates the euclidian distance between two vec3's
     *
     * @param a the first operand
     * @param b the second operand
     * @returns distance between a and b
     */
    export function dist(a: GLMVec3, b: GLMVec3): number;
    
    /**
     * Calculates the squared euclidian distance between two vec3's
     *
     * @param a the first operand
     * @param b the second operand
     * @returns squared distance between a and b
     */
    export function squaredDistance(a: GLMVec3, b: GLMVec3): number;
    
    /**
     * Calculates the squared euclidian distance between two vec3's
     *
     * @param a the first operand
     * @param b the second operand
     * @returns squared distance between a and b
     */
    export function sqrDist(a: GLMVec3, b: GLMVec3): number;
    
    /**
     * Calculates the length of a vec3
     *
     * @param a vector to calculate length of
     * @returns length of a
     */
    export function length(a: GLMVec3): number;  
    
    /**
     * Calculates the length of a vec3
     *
     * @param a vector to calculate length of
     * @returns length of a
     */
    export function len(a: GLMVec3): number;
    
    /**
     * Calculates the squared length of a vec3
     *
     * @param a vector to calculate squared length of
     * @returns squared length of a
     */
    export function squaredLength(a: GLMVec3): number;
    
    /**
     * Calculates the squared length of a vec3
     *
     * @param a vector to calculate squared length of
     * @returns squared length of a
     */
    export function sqrLen(a: GLMVec3): number;  
    
    /**
     * Negates the components of a vec3
     *
     * @param out the receiving vector
     * @param a vector to negate
     * @returns out
     */
    export function negate(out: GLMVec3, a: GLMVec3): GLMVec3;
    
    /**
     * Returns the inverse of the components of a vec3
     *
     * @param out the receiving vector
     * @param a vector to invert
     * @returns out
     */
    export function inverse(out: GLMVec3, a: GLMVec3): GLMVec3;
    
    /**
     * Normalize a vec3
     *
     * @param out the receiving vector
     * @param a vector to normalize
     * @returns out
     */
    export function normalize(out: GLMVec3, a: GLMVec3): GLMVec3;
    
    /**
     * Calculates the dot product of two vec3's
     *
     * @param a the first operand
     * @param b the second operand
     * @returns dot product of a and b
     */
    export function dot(a: GLMVec3, b: GLMVec3): number;  
    
    /**
     * Computes the cross product of two vec3's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function cross(out: GLMVec3, a: GLMVec3, b: GLMVec3): GLMVec3;
    
    /**
     * Performs a linear interpolation between two vec3's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @param t interpolation amount between the two inputs
     * @returns out
     */
    export function lerp(out: GLMVec3, a: GLMVec3, b: GLMVec3, t: number): GLMVec3;
    
    /**
     * Generates a random unit vector
     *
     * @param out the receiving vector
     * @returns out
     */
    export function random(out: GLMVec3): GLMVec3;
    
    /**
     * Generates a random vector with the given scale
     *
     * @param out the receiving vector
     * @param [scale] Length of the resulting vector. If ommitted, a unit vector will be returned
     * @returns out
     */
    export function random(out: GLMVec3, scale: number): GLMVec3;
    
    /**
     * Rotate a 3D vector around the x-axis
     * @param out The receiving vec3
     * @param a The vec3 point to rotate
     * @param b The origin of the rotation
     * @param c The angle of rotation
     * @returns out
     */
    export function rotateX(out: GLMVec3, a: GLMVec3, b: GLMVec3, c: number): GLMVec3;
    
    /**
     * Rotate a 3D vector around the y-axis
     * @param out The receiving vec3
     * @param a The vec3 point to rotate
     * @param b The origin of the rotation
     * @param c The angle of rotation
     * @returns out
     */
    export function rotateY(out: GLMVec3, a: GLMVec3, b: GLMVec3, c: number): GLMVec3;
    
    /**
     * Rotate a 3D vector around the z-axis
     * @param out The receiving vec3
     * @param a The vec3 point to rotate
     * @param b The origin of the rotation
     * @param c The angle of rotation
     * @returns out
     */
    export function rotateZ(out: GLMVec3, a: GLMVec3, b: GLMVec3, c: number): GLMVec3;
    
    /**
     * Transforms the vec3 with a mat3.
     *
     * @param out the receiving vector
     * @param a the vector to transform
     * @param m the 3x3 matrix to transform with
     * @returns out
     */
    export function transformMat3(out: GLMVec3, a: GLMVec3, m: GLMMat3): GLMVec3;
    
    /**
     * Transforms the vec3 with a mat4.
     * 4th vector component is implicitly '1'
     *
     * @param out the receiving vector
     * @param a the vector to transform
     * @param m matrix to transform with
     * @returns out
     */
    export function transformMat4(out: GLMVec3, a: GLMVec3, m: GLMMat4): GLMVec3;  
    
    /**
     * Transforms the vec3 with a quat
     *
     * @param out the receiving vector
     * @param a the vector to transform
     * @param q quaternion to transform with
     * @returns out
     */
    export function transformQuat(out: GLMVec3, a: GLMVec3, q: GLM.IArray): GLMVec3;
    
    
    /**
     * Perform some operation over an array of vec3s.
     *
     * @param a the array of vectors to iterate over
     * @param stride Number of elements between the start of each vec3. If 0 assumes tightly packed
     * @param offset Number of elements to skip at the beginning of the array
     * @param count Number of vec3s to iterate over. If 0 iterates over entire array
     * @param fn Function to call for each vector in the array
     * @param arg additional argument to pass to fn
     * @returns a
     * @function
     */
    export function forEach(out: GLM.IArray, string: number, offset: number, count: number,
        fn: (a: GLM.IArray, b: GLM.IArray, arg: any) => void, arg: any): GLM.IArray;
        
    /**
     * Perform some operation over an array of vec3s.
     *
     * @param a the array of vectors to iterate over
     * @param stride Number of elements between the start of each vec3. If 0 assumes tightly packed
     * @param offset Number of elements to skip at the beginning of the array
     * @param count Number of vec3s to iterate over. If 0 iterates over entire array
     * @param fn Function to call for each vector in the array
     * @returns a
     * @function
     */
    export function forEach(out: GLM.IArray, string: number, offset: number, count: number,
        fn: (a: GLM.IArray, b: GLM.IArray) => void): GLM.IArray;
        
    /**
     * Get the angle between two 3D vectors
     * @param a The first operand
     * @param b The second operand
     * @returns The angle in radians
     */
    export function angle(a: GLMVec3, b: GLMVec3): number;
    
    /**
     * Returns a string representation of a vector
     *
     * @param vec vector to represent as a string
     * @returns string representation of the vector
     */
    export function str(a: GLMVec3): string;  
}

// vec4 
declare module vec4 {
    
    /**
     * Creates a new, empty vec4
     *
     * @returns a new 4D vector
     */
    export function create(): GLMVec4;
    
    /**
     * Creates a new vec4 initialized with values from an existing vector
     *
     * @param a vector to clone
     * @returns a new 4D vector
     */
    export function clone(a: GLMVec4): GLMVec4;
    
    /**
     * Creates a new vec4 initialized with the given values
     *
     * @param x X component
     * @param y Y component
     * @param z Z component
     * @param w W component
     * @returns a new 4D vector
     */
    export function fromValues(x: number, y: number, z: number, w: number): GLMVec4;
    
    /**
     * Copy the values from one vec4 to another
     *
     * @param out the receiving vector
     * @param a the source vector
     * @returns out
     */
    export function copy(out: GLMVec4, a: GLMVec4): GLMVec4;  
    
    /**
     * Set the components of a vec4 to the given values
     *
     * @param out the receiving vector
     * @param x X component
     * @param y Y component
     * @param z Z component
     * @param w W component
     * @returns out
     */
    export function set(out: GLMVec4, x: number, y: number, z: number, w: number): GLMVec4;  
    
    /**
     * Adds two vec4's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function add(out: GLMVec4, a: GLMVec4, b: GLMVec4): GLMVec4;
    
    /**
     * Subtracts vector b from vector a
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function subtract(out: GLMVec4, a: GLMVec4, b: GLMVec4): GLMVec4;
    
    /**
     * Subtracts vector b from vector a
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function sub(out: GLMVec4, a: GLMVec4, b: GLMVec4): GLMVec4;
    
    /**
     * Multiplies two vec4's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function multiply(out: GLMVec4, a: GLMVec4, b: GLMVec4): GLMVec4;
    
    /**
     * Multiplies two vec4's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function mul(out: GLMVec4, a: GLMVec4, b: GLMVec4): GLMVec4;
    
    /**
     * Divides two vec4's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function divide(out: GLMVec4, a: GLMVec4, b: GLMVec4): GLMVec4;
   
    /**
     * Divides two vec4's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function div(out: GLMVec4, a: GLMVec4, b: GLMVec4): GLMVec4;  
    
    /**
     * Returns the minimum of two vec4's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function min(out: GLMVec4, a: GLMVec4, b: GLMVec4): GLMVec4;
    
    /**
     * Returns the maximum of two vec4's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function max(out: GLMVec4, a: GLMVec4, b: GLMVec4): GLMVec4;  
    
    /**
     * Scales a vec4 by a scalar number
     *
     * @param out the receiving vector
     * @param a the vector to scale
     * @param b amount to scale the vector by
     * @returns out
     */
    export function scale(out: GLMVec4, a: GLMVec4, b: number): GLMVec4;
    
    /**
     * Adds two vec4's after scaling the second operand by a scalar value
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @param scale the amount to scale b by before adding
     * @returns out
     */
    export function scaleAndAdd(out: GLMVec4, a: GLMVec4, b: GLMVec4, scale: number): GLMVec4;
    
    /**
     * Calculates the euclidian distance between two vec4's
     *
     * @param a the first operand
     * @param b the second operand
     * @returns distance between a and b
     */
    export function distance(a: GLMVec4, b: GLMVec4): number;
    
    /**
     * Calculates the euclidian distance between two vec4's
     *
     * @param a the first operand
     * @param b the second operand
     * @returns distance between a and b
     */
    export function dist(a: GLMVec4, b: GLMVec4): number;
    
    /**
     * Calculates the squared euclidian distance between two vec4's
     *
     * @param a the first operand
     * @param b the second operand
     * @returns squared distance between a and b
     */
    export function squaredDistance(a: GLMVec4, b: GLMVec4): number;
    
    /**
     * Calculates the squared euclidian distance between two vec4's
     *
     * @param a the first operand
     * @param b the second operand
     * @returns squared distance between a and b
     */
    export function sqrDist(a: GLMVec4, b: GLMVec4): number;
    
    /**
     * Calculates the length of a vec4
     *
     * @param a vector to calculate length of
     * @returns length of a
     */
    export function length(a: GLMVec4): number;  
    
    /**
     * Calculates the length of a vec4
     *
     * @param a vector to calculate length of
     * @returns length of a
     */
    export function len(a: GLMVec4): number;
    
    /**
     * Calculates the squared length of a vec4
     *
     * @param a vector to calculate squared length of
     * @returns squared length of a
     */
    export function squaredLength(a: GLMVec4): number;
    
    /**
     * Calculates the squared length of a vec4
     *
     * @param a vector to calculate squared length of
     * @returns squared length of a
     */
    export function sqrLen(a: GLMVec4): number;  
    
    /**
     * Negates the components of a vec4
     *
     * @param out the receiving vector
     * @param a vector to negate
     * @returns out
     */
    export function negate(out: GLMVec4, a: GLMVec4): GLMVec4;
    
    /**
     * Returns the inverse of the components of a vec4
     *
     * @param out the receiving vector
     * @param a vector to invert
     * @returns out
     */
    export function inverse(out: GLMVec4, a: GLMVec4): GLMVec4;
    
    /**
     * Normalize a vec4
     *
     * @param out the receiving vector
     * @param a vector to normalize
     * @returns out
     */
    export function normalize(out: GLMVec4, a: GLMVec4): GLMVec4;
    
    /**
     * Calculates the dot product of two vec4's
     *
     * @param a the first operand
     * @param b the second operand
     * @returns dot product of a and b
     */
    export function dot(a: GLMVec4, b: GLMVec4): number;
    
    /**
     * Performs a linear interpolation between two vec4's
     *
     * @param out the receiving vector
     * @param a the first operand
     * @param b the second operand
     * @param t interpolation amount between the two inputs
     * @returns out
     */
    export function lerp(out: GLMVec4, a: GLMVec4, b: GLMVec4, t: number): GLMVec4;
    
    /**
     * Generates a random unit vector
     *
     * @param out the receiving vector
     * @returns out
     */
    export function random(out: GLMVec4): GLMVec4;
    
    /**
     * Generates a random vector with the given scale
     *
     * @param out the receiving vector
     * @param Length of the resulting vector. If ommitted, a unit vector will be returned
     * @returns out
     */
    export function random(out: GLMVec4, scale: number): GLMVec4; 
    
    /**
     * Transforms the vec4 with a mat4.
     *
     * @param out the receiving vector
     * @param a the vector to transform
     * @param m matrix to transform with
     * @returns out
     */
    export function transformMat4(out: GLMVec4, a: GLMVec4, mat: GLMVec4): GLMVec4;
    
    /**
     * Transforms the vec4 with a quat
     *
     * @param out the receiving vector
     * @param a the vector to transform
     * @param q quaternion to transform with
     * @returns out
     */
    export function transformQuat(out: GLMVec4, a: GLMVec4, quat: GLMVec4): GLMVec4;   
    
    /**
     * Perform some operation over an array of vec4s.
     *
     * @param a the array of vectors to iterate over
     * @param stride Number of elements between the start of each vec4. If 0 assumes tightly packed
     * @param offset Number of elements to skip at the beginning of the array
     * @param count Number of vec4s to iterate over. If 0 iterates over entire array
     * @param fn Function to call for each vector in the array
     * @param additional argument to pass to fn
     * @returns a
     * @function
     */
    export function forEach(out: GLM.IArray, string: number, offset: number, count: number,
        callback: (a: GLM.IArray, b: GLM.IArray, arg: any) => void, arg: any): GLM.IArray;  
        
    /**
     * Perform some operation over an array of vec4s.
     *
     * @param a the array of vectors to iterate over
     * @param stride Number of elements between the start of each vec4. If 0 assumes tightly packed
     * @param offset Number of elements to skip at the beginning of the array
     * @param count Number of vec4s to iterate over. If 0 iterates over entire array
     * @param fn Function to call for each vector in the array
     * @returns a
     * @function
     */
    export function forEach(out: GLM.IArray, string: number, offset: number, count: number,
        callback: (a: GLM.IArray, b: GLM.IArray) => void): GLM.IArray;
        
    /**
     * Returns a string representation of a vector
     *
     * @param vec vector to represent as a string
     * @returns string representation of the vector
     */
    export function str(a: GLMVec4): string;   
}

// mat2
declare module mat2 {
    
    /**
     * Creates a new identity mat2
     *
     * @returns a new 2x2 matrix
     */
    export function create(): GLM.IArray;
    
    /**
     * Creates a new mat2 initialized with values from an existing matrix
     *
     * @param a matrix to clone
     * @returns a new 2x2 matrix
     */
    export function clone(a: GLM.IArray): GLM.IArray;
    
    /**
     * Copy the values from one mat2 to another
     *
     * @param out the receiving matrix
     * @param a the source matrix
     * @returns out
     */
    export function copy(out: GLM.IArray, a: GLM.IArray): GLM.IArray;
    
    /**
     * Set a mat2 to the identity matrix
     *
     * @param out the receiving matrix
     * @returns out
     */
    export function identity(out: GLM.IArray): GLM.IArray;
    
    /**
     * Transpose the values of a mat2
     *
     * @param out the receiving matrix
     * @param a the source matrix
     * @returns out
     */
    export function transpose(out: GLM.IArray, a: GLM.IArray): GLM.IArray;
    
    /**
     * Inverts a mat2
     *
     * @param out the receiving matrix
     * @param a the source matrix
     * @returns out
     */
    export function invert(out: GLM.IArray, a: GLM.IArray): GLM.IArray;
    
    /**
     * Calculates the adjugate of a mat2
     *
     * @param out the receiving matrix
     * @param a the source matrix
     * @returns out
     */
    export function adjoint(out: GLM.IArray, a: GLM.IArray): GLM.IArray;
    
    /**
     * Calculates the determinant of a mat2
     *
     * @param a the source matrix
     * @returns determinant of a
     */
    export function determinant(a: GLM.IArray): number;
    
    /**
     * Multiplies two mat2's
     *
     * @param out the receiving matrix
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function multiply(out: GLM.IArray, a: GLM.IArray, b: GLM.IArray): GLM.IArray;
    
    /**
     * Multiplies two mat2's
     *
     * @param out the receiving matrix
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function mul(out: GLM.IArray, a: GLM.IArray, b: GLM.IArray): GLM.IArray;
    
    /**
     * Rotates a mat2 by the given angle
     *
     * @param out the receiving matrix
     * @param a the matrix to rotate
     * @param rad the angle to rotate the matrix by
     * @returns out
     */
    export function rotate(out: GLM.IArray, a: GLM.IArray, rad: number): GLM.IArray;
    
    /**
     * Scales the mat2 by the dimensions in the given vec2
     *
     * @param out the receiving matrix
     * @param a the matrix to rotate
     * @param v the vec2 to scale the matrix by
     * @returns out
     **/
    export function scale(out: GLM.IArray, a: GLM.IArray, v: GLM.IArray): GLM.IArray;
    
    /**
     * Returns a string representation of a mat2
     *
     * @param a matrix to represent as a string
     * @returns string representation of the matrix
     */
    export function str(a: GLM.IArray): string;
    
    /**
     * Returns Frobenius norm of a mat2
     *
     * @param a the matrix to calculate Frobenius norm of
     * @returns Frobenius norm
     */
    export function frob(a: GLM.IArray): number;
    
    /**
     * Returns L, D and U matrices (Lower triangular, Diagonal and Upper triangular) by factorizing the input matrix
     * @param L the lower triangular matrix 
     * @param D the diagonal matrix 
     * @param U the upper triangular matrix 
     * @param a the input matrix to factorize
     */
    export function LDU(L: GLM.IArray, D: GLM.IArray, U: GLM.IArray, a: GLM.IArray): GLM.IArray;
}

// mat2d
declare module mat2d {
    
    /**
     * Creates a new identity mat2d
     *
     * @returns a new 2x3 matrix
     */
    export function create(): GLMMat2;
    
    /**
     * Creates a new mat2d initialized with values from an existing matrix
     *
     * @param a matrix to clone
     * @returns a new 2x3 matrix
     */
    export function clone(a: GLMMat2): GLMMat2;
    
    /**
     * Copy the values from one mat2d to another
     *
     * @param out the receiving matrix
     * @param a the source matrix
     * @returns out
     */
    export function copy(out: GLMMat2, a: GLMMat2): GLMMat2;
    
    /**
     * Set a mat2d to the identity matrix
     *
     * @param out the receiving matrix
     * @returns out
     */
    export function identity(out: GLMMat2): GLMMat2;  
    
    /**
     * Inverts a mat2d
     *
     * @param out the receiving matrix
     * @param a the source matrix
     * @returns out
     */
    export function invert(out: GLMMat2, a: GLMMat2): GLMMat2;
    
    /**
     * Calculates the determinant of a mat2d
     *
     * @param a the source matrix
     * @returns determinant of a
     */
    export function determinant(a: GLMMat2): number;
    
    /**
     * Multiplies two mat2d's
     *
     * @param out the receiving matrix
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function multiply(out: GLMMat2, a: GLMMat2, b: GLMMat2): GLMMat2;
    
    /**
     * Multiplies two mat2d's
     *
     * @param out the receiving matrix
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function mul(out: GLMMat2, a: GLMMat2, b: GLMMat2): GLMMat2; 
    
    /**
     * Rotates a mat2d by the given angle
     *
     * @param out the receiving matrix
     * @param a the matrix to rotate
     * @param rad the angle to rotate the matrix by
     * @returns out
     */
    export function rotate(out: GLMMat2, a: GLMMat2, rad: number): GLMMat2; 
    
    /**
     * Scales the mat2d by the dimensions in the given vec2
     *
     * @param out the receiving matrix
     * @param a the matrix to translate
     * @param v the vec2 to scale the matrix by
     * @returns out
     **/
    export function scale(out: GLMMat2, a: GLMMat2, v: GLMVec2): GLMMat2;
    
    /**
     * Translates the mat2d by the dimensions in the given vec2
     *
     * @param out the receiving matrix
     * @param a the matrix to translate
     * @param v the vec2 to translate the matrix by
     * @returns out
     **/
    export function translate(out: GLMMat2, a: GLMMat2, v: GLMVec2): GLMMat2;
    
    /**
     * Returns a string representation of a mat2d
     *
     * @param a matrix to represent as a string
     * @returns string representation of the matrix
     */
    export function str(a: GLMMat2): string;
    
    /**
     * Returns Frobenius norm of a mat2d
     *
     * @param a the matrix to calculate Frobenius norm of
     * @returns Frobenius norm
     */
    export function frob(a: GLMMat2): number;
}

// mat3
declare module mat3 {
    
    /**
     * Creates a new identity mat3
     *
     * @returns a new 3x3 matrix
     */
    export function create(): GLMMat3;
    
    /**
     * Creates a new mat3 initialized with values from an existing matrix
     *
     * @param a matrix to clone
     * @returns a new 3x3 matrix
     */
    export function clone(a: GLMMat3): GLMMat3;
    
    /**
     * Copy the values from one mat3 to another
     *
     * @param out the receiving matrix
     * @param a the source matrix
     * @returns out
     */
    export function copy(out: GLMMat3, a: GLMMat3): GLMMat3;
    
    /**
     * Set a mat3 to the identity matrix
     *
     * @param out the receiving matrix
     * @returns out
     */
    export function identity(out: GLMMat3): GLMMat3;
    
    /**
     * Transpose the values of a mat3
     *
     * @param out the receiving matrix
     * @param a the source matrix
     * @returns out
     */
    export function transpose(out: GLMMat3, a: GLMMat3): GLMMat3;
    
    /**
     * Inverts a mat3
     *
     * @param out the receiving matrix
     * @param a the source matrix
     * @returns out
     */
    export function invert(out: GLMMat3, a: GLMMat3): GLMMat3;
    
    /**
     * Calculates the adjugate of a mat3
     *
     * @param out the receiving matrix
     * @param a the source matrix
     * @returns out
     */
    export function adjoint(out: GLMMat3, a: GLMMat3): GLMMat3;
    
    /**
     * Calculates the determinant of a mat3
     *
     * @param a the source matrix
     * @returns determinant of a
     */
    export function determinant(a: GLMMat3): number;
    
    /**
     * Multiplies two mat3's
     *
     * @param out the receiving matrix
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function multiply(out: GLMMat3, a: GLMMat3, b: GLMMat3): GLMMat3;
    
    /**
     * Multiplies two mat3's
     *
     * @param out the receiving matrix
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function mul(out: GLMMat3, a: GLMMat3, b: GLMMat3): GLMMat3;
    
    /**
     * Returns a string representation of a mat3
     *
     * @param mat matrix to represent as a string
     * @returns string representation of the matrix
     */
    export function str(mat: GLMMat3): string;
    
    /**
     * Returns Frobenius norm of a mat3
     *
     * @param a the matrix to calculate Frobenius norm of
     * @returns Frobenius norm
     */
    export function frob(a: GLMMat3): number;  
    
    /**
    * Calculates a 3x3 normal matrix (transpose inverse) from the 4x4 matrix
    *
    * @param out mat3 receiving operation result
    * @param a Mat4 to derive the normal matrix from
    *
    * @returns out
    */
    export function normalFromMat4(out: GLMMat3, a: GLMMat4): GLMMat3;
    
    /**
    * Calculates a 3x3 matrix from the given quaternion
    *
    * @param out mat3 receiving operation result
    * @param q Quaternion to create matrix from
    *
    * @returns out
    */
    export function fromQuat(out: GLMMat3, q: GLM.IArray): GLMMat3;
    
    /**
     * Copies the upper-left 3x3 values into the given mat3.
     *
     * @param out the receiving 3x3 matrix
     * @param a   the source 4x4 matrix
     * @returns out
     */
    export function fromMat4(out: GLMMat3, a: GLMMat4): GLMMat3;
    
    /**
     * Scales the mat3 by the dimensions in the given vec2
     *
     * @param out the receiving matrix
     * @param a the matrix to rotate
     * @param v the vec2 to scale the matrix by
     * @returns out
     **/
    export function scale(out: GLMMat3, a: GLMMat3, v: GLMVec2): GLMMat3;
    
    /**
     * Copies the values from a mat2d into a mat3
     *
     * @param out the receiving matrix
     * @param {mat2d} a the matrix to copy
     * @returns out
     **/
    export function fromMat2d(out: GLMMat3, a: GLMMat2d): GLMMat3;
    
    /**
     * Translate a mat3 by the given vector
     *
     * @param out the receiving matrix
     * @param a the matrix to translate
     * @param v vector to translate by
     * @returns out
     */
     export function translate(out: GLMMat3, a: GLMMat3, v: GLMVec2): GLMMat3;
     
    /**
     * Rotates a mat3 by the given angle
     *
     * @param out the receiving matrix
     * @param a the matrix to rotate
     * @param rad the angle to rotate the matrix by
     * @returns out
     */
     export function rotate(out: GLMMat3, a: GLMMat3, rad: number): GLMMat3;
}

// mat4
declare module mat4 {
    
    /**
     * Creates a new identity mat4
     *
     * @returns a new 4x4 matrix
     */
    export function create(): GLMMat4;
    
    /**
     * Creates a new mat4 initialized with values from an existing matrix
     *
     * @param a matrix to clone
     * @returns a new 4x4 matrix
     */
    export function clone(a: GLMMat4): GLMMat4;
    
    /**
     * Copy the values from one mat4 to another
     *
     * @param out the receiving matrix
     * @param a the source matrix
     * @returns out
     */
    export function copy(out: GLMMat4, a: GLMMat4): GLMMat4;
    
    /**
     * Set a mat4 to the identity matrix
     *
     * @param out the receiving matrix
     * @returns out
     */
    export function identity(a: GLMMat4): GLMMat4;
    
    /**
     * Transpose the values of a mat4
     *
     * @param out the receiving matrix
     * @param a the source matrix
     * @returns out
     */
    export function transpose(out: GLMMat4, a: GLMMat4): GLMMat4; 
    
    /**
     * Inverts a mat4
     *
     * @param out the receiving matrix
     * @param a the source matrix
     * @returns out
     */
    export function invert(out: GLMMat4, a: GLMMat4): GLMMat4;
    
    /**
     * Calculates the adjugate of a mat4
     *
     * @param out the receiving matrix
     * @param a the source matrix
     * @returns out
     */
    export function adjoint(out: GLMMat4, a: GLMMat4): GLMMat4;
    
    /**
     * Calculates the determinant of a mat4
     *
     * @param a the source matrix
     * @returns determinant of a
     */
    export function determinant(a: GLMMat4): number;
    
    /**
     * Multiplies two mat4's
     *
     * @param out the receiving matrix
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function multiply(out: GLMMat4, a: GLMMat4, b: GLMMat4): GLMMat4;
    
    /**
     * Multiplies two mat4's
     *
     * @param out the receiving matrix
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function mul(out: GLMMat4, a: GLMMat4, b: GLMMat4): GLMMat4;
    
    /**
     * Translate a mat4 by the given vector
     *
     * @param out the receiving matrix
     * @param a the matrix to translate
     * @param v vector to translate by
     * @returns out
     */
    export function translate(out: GLMMat4, a: GLMMat4, v: GLMVec3): GLMMat4;
    
    /**
     * Scales the mat4 by the dimensions in the given vec3
     *
     * @param out the receiving matrix
     * @param a the matrix to scale
     * @param v the vec3 to scale the matrix by
     * @returns out
     **/
    export function scale(out: GLMMat4, a: GLMMat4, v: GLMVec3): GLMMat4;
    
    /**
     * Rotates a mat4 by the given angle
     *
     * @param out the receiving matrix
     * @param a the matrix to rotate
     * @param rad the angle to rotate the matrix by
     * @param axis the axis to rotate around
     * @returns out
     */
    export function rotate(out: GLMMat4, a: GLMMat4, rad: number, axis: GLMVec3): GLMMat4;
    
    /**
     * Rotates a matrix by the given angle around the X axis
     *
     * @param out the receiving matrix
     * @param a the matrix to rotate
     * @param rad the angle to rotate the matrix by
     * @returns out
     */
    export function rotateX(out: GLMMat4, a: GLMMat4, rad: number): GLMMat4;
    
    /**
     * Rotates a matrix by the given angle around the Y axis
     *
     * @param out the receiving matrix
     * @param a the matrix to rotate
     * @param rad the angle to rotate the matrix by
     * @returns out
     */
    export function rotateY(out: GLMMat4, a: GLMMat4, rad: number): GLMMat4;
    
    /**
     * Rotates a matrix by the given angle around the Z axis
     *
     * @param out the receiving matrix
     * @param a the matrix to rotate
     * @param rad the angle to rotate the matrix by
     * @returns out
     */
    export function rotateZ(out: GLMMat4, a: GLMMat4, rad: number): GLMMat4;
    
    /**
     * Generates a frustum matrix with the given bounds
     *
     * @param out mat4 frustum matrix will be written into
     * @param left Left bound of the frustum
     * @param right Right bound of the frustum
     * @param bottom Bottom bound of the frustum
     * @param top Top bound of the frustum
     * @param near Near bound of the frustum
     * @param far Far bound of the frustum
     * @returns out
     */
    export function frustum(out: GLMMat4, left: number, right: number,
        bottom: number, top: number, near: number, far: number): GLMMat4;
        
    /**
     * Generates a perspective projection matrix with the given bounds
     *
     * @param out mat4 frustum matrix will be written into
     * @param fovy Vertical field of view in radians
     * @param aspect Aspect ratio. typically viewport width/height
     * @param near Near bound of the frustum
     * @param far Far bound of the frustum
     * @returns out
     */
    export function perspective(out: GLMMat4, fovy: number, aspect: number,
        near: number, far: number): GLMMat4;
        
    /**
     * Generates a orthogonal projection matrix with the given bounds
     *
     * @param out mat4 frustum matrix will be written into
     * @param left Left bound of the frustum
     * @param right Right bound of the frustum
     * @param bottom Bottom bound of the frustum
     * @param top Top bound of the frustum
     * @param near Near bound of the frustum
     * @param far Far bound of the frustum
     * @returns out
     */
    export function ortho(out: GLMMat4, left: number, right: number,
        bottom: number, top: number, near: number, far: number): GLMMat4;
        
    /**
     * Generates a look-at matrix with the given eye position, focal point, and up axis
     *
     * @param out mat4 frustum matrix will be written into
     * @param eye Position of the viewer
     * @param center Point the viewer is looking at
     * @param up vec3 pointing up
     * @returns out
     */
    export function lookAt(out: GLMMat4, eye: GLMVec3, 
        center: GLMVec3, up: GLMVec3): GLMMat4;
        
    /**
     * Returns a string representation of a mat4
     *
     * @param mat matrix to represent as a string
     * @returns string representation of the matrix
     */
    export function str(mat: GLMMat4): string;
    
    /**
     * Returns Frobenius norm of a mat4
     *
     * @param a the matrix to calculate Frobenius norm of
     * @returns Frobenius norm
     */
    export function frob(a: GLMMat4): number;
    
    /**
     * Creates a matrix from a quaternion rotation and vector translation
     * This is equivalent to (but much faster than):
     *
     *     mat4.identity(dest);
     *     mat4.translate(dest, vec);
     *     var quatMat = mat4.create();
     *     quat4.toMat4(quat, quatMat);
     *     mat4.multiply(dest, quatMat);
     *
     * @param out mat4 receiving operation result
     * @param q Rotation quaternion
     * @param v Translation vector
     * @returns out
     */
    export function fromRotationTranslation(out: GLMMat4, q: GLM.IArray,
        v: GLMVec3): GLMMat4;
        
    /**
     * Creates a matrix from a quaternion
     *
     * @param out mat4 receiving operation result
     * @param q Rotation quaternion
     * @returns out
     */
    export function fromQuat(out: GLMMat4, q: GLM.IArray): GLMMat4;
}

// quat
declare module quat {
    
    /**
     * Creates a new identity quat
     *
     * @returns a new quaternion
     */
    export function create(): GLMQuat;
    
    /**
     * Creates a new quat initialized with values from an existing quaternion
     *
     * @param a quaternion to clone
     * @returns a new quaternion
     * @function
     */
    export function clone(a: GLMQuat): GLMQuat;
    
    /**
     * Creates a new quat initialized with the given values
     *
     * @param x X component
     * @param y Y component
     * @param z Z component
     * @param w W component
     * @returns a new quaternion
     * @function
     */
    export function fromValues(x: number, y: number, z: number, w: number): GLMQuat;
    
    /**
     * Copy the values from one quat to another
     *
     * @param out the receiving quaternion
     * @param a the source quaternion
     * @returns out
     * @function
     */
    export function copy(out: GLMQuat, a: GLMQuat): GLMQuat;
    
    /**
     * Set the components of a quat to the given values
     *
     * @param out the receiving quaternion
     * @param x X component
     * @param y Y component
     * @param z Z component
     * @param w W component
     * @returns out
     * @function
     */
    export function set(out: GLMQuat, x: number, y: number, z: number, w: number): GLMQuat;
    
    /**
     * Set a quat to the identity quaternion
     *
     * @param out the receiving quaternion
     * @returns out
     */
    export function identity(out: GLMQuat): GLMQuat;
    
    /**
     * Sets a quat from the given angle and rotation axis,
     * then returns it.
     *
     * @param out the receiving quaternion
     * @param axis the axis around which to rotate
     * @param rad the angle in radians
     * @returns out
     **/
    export function setAxisAngle(out: GLMQuat, axis: GLMQuat, rad: number): GLMQuat;
    
    /**
     * Adds two quat's
     *
     * @param out the receiving quaternion
     * @param a the first operand
     * @param b the second operand
     * @returns out
     * @function
     */
    export function add(out: GLMQuat, a: GLMQuat, b: GLMQuat): GLMQuat;
    
    /**
     * Multiplies two quat's
     *
     * @param out the receiving quaternion
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function multiply(out: GLMQuat, a: GLMQuat, b: GLMQuat): GLMQuat;
    
    /**
     * Multiplies two quat's
     *
     * @param out the receiving quaternion
     * @param a the first operand
     * @param b the second operand
     * @returns out
     */
    export function mul(out: GLMQuat, a: GLMQuat, b: GLMQuat): GLMQuat;
    
    /**
     * Scales a quat by a scalar number
     *
     * @param out the receiving vector
     * @param a the vector to scale
     * @param b amount to scale the vector by
     * @returns out
     * @function
     */
    export function scale(out: GLMQuat, a: GLMQuat, b: number): GLMQuat;
    
    /**
     * Calculates the length of a quat
     *
     * @param a vector to calculate length of
     * @returns length of a
     * @function
     */
    export function length(a: GLMQuat): number;
    
    /**
     * Calculates the length of a quat
     *
     * @param a vector to calculate length of
     * @returns length of a
     * @function
     */
    export function len(a: GLMQuat): number;
    
    /**
     * Calculates the squared length of a quat
     *
     * @param a vector to calculate squared length of
     * @returns squared length of a
     * @function
     */
    export function squaredLength(a: GLMQuat): number;
    
    /**
     * Calculates the squared length of a quat
     *
     * @param a vector to calculate squared length of
     * @returns squared length of a
     * @function
     */
    export function sqrLen(a: GLMQuat): number;
    
    /**
     * Normalize a quat
     *
     * @param out the receiving quaternion
     * @param a quaternion to normalize
     * @returns out
     * @function
     */
    export function normalize(out: GLMQuat, a: GLMQuat): GLMQuat;
    
    /**
     * Calculates the dot product of two quat's
     *
     * @param a the first operand
     * @param b the second operand
     * @returns dot product of a and b
     * @function
     */
    export function dot(out: GLMQuat, a: GLMQuat, b: GLMQuat): number;
    
    /**
     * Performs a linear interpolation between two quat's
     *
     * @param out the receiving quaternion
     * @param a the first operand
     * @param b the second operand
     * @param t interpolation amount between the two inputs
     * @returns out
     * @function
     */
    export function lerp(out: GLMQuat, a: GLMQuat, b: GLMQuat, t: number): GLMQuat;
    
    /**
     * Performs a spherical linear interpolation between two quat
     *
     * @param out the receiving quaternion
     * @param a the first operand
     * @param b the second operand
     * @param t interpolation amount between the two inputs
     * @returns out
     */
    export function slerp(out: GLMQuat, a: GLMQuat, b: GLMQuat, t: number): GLMQuat;
    
    /**
     * Calculates the inverse of a quat
     *
     * @param out the receiving quaternion
     * @param a quat to calculate inverse of
     * @returns out
     */
    export function invert(out: GLMQuat, a: GLMQuat): GLMQuat;
    
    /**
     * Calculates the conjugate of a quat
     * If the quaternion is normalized, this function is faster than quat.inverse and produces the same result.
     *
     * @param out the receiving quaternion
     * @param a quat to calculate conjugate of
     * @returns out
     */
    export function conjugate(out: GLMQuat, a: GLMQuat): GLMQuat;
    
    /**
     * Returns a string representation of a quatenion
     *
     * @param vec vector to represent as a string
     * @returns string representation of the vector
     */
    export function str(a: GLMQuat): string;
    
    /**
     * Rotates a quaternion by the given angle about the X axis
     *
     * @param out quat receiving operation result
     * @param a quat to rotate
     * @param rad angle (in radians) to rotate
     * @returns out
     */
    export function rotateX(out: GLMQuat, a: GLMQuat, rad: number): GLMQuat;
    
    /**
     * Rotates a quaternion by the given angle about the Y axis
     *
     * @param out quat receiving operation result
     * @param a quat to rotate
     * @param rad angle (in radians) to rotate
     * @returns out
     */
    export function rotateY(out: GLMQuat, a: GLMQuat, rad: number): GLMQuat;
    
    /**
     * Rotates a quaternion by the given angle about the Z axis
     *
     * @param out quat receiving operation result
     * @param a quat to rotate
     * @param rad angle (in radians) to rotate
     * @returns out
     */
    export function rotateZ(out: GLMQuat, a: GLMQuat, rad: number): GLMQuat;
    
    /**
     * Creates a quaternion from the given 3x3 rotation matrix.
     *
     * NOTE: The resultant quaternion is not normalized, so you should be sure
     * to renormalize the quaternion yourself where necessary.
     *
     * @param out the receiving quaternion
     * @param m rotation matrix
     * @returns out
     * @function
     */
    export function fromMat3(out: GLMQuat, m: GLMMat3): GLMQuat;
    
    /**
     * Sets the specified quaternion with values corresponding to the given
     * axes. Each axis is a vec3 and is expected to be unit length and
     * perpendicular to all other specified axes.
     *
     * @param view  the vector representing the viewing direction
     * @param right the vector representing the local "right" direction
     * @param up    the vector representing the local "up" direction
     * @returns out
     */
    export function setAxes(out: GLMQuat, view: GLMVec3, right: GLMVec3, 
        up: GLMVec3): GLMQuat;
        
    /**
     * Sets a quaternion to represent the shortest rotation from one
     * vector to another.
     *
     * Both vectors are assumed to be unit length.
     *
     * @param out the receiving quaternion.
     * @param a the initial vector
     * @param b the destination vector
     * @returns out
     */
    export function rotationTo(out: GLMQuat, a: GLMVec3, b: GLMVec3): GLMQuat;
    
    /**
     * Calculates the W component of a quat from the X, Y, and Z components.
     * Assumes that quaternion is 1 unit in length.
     * Any existing W component will be ignored.
     *
     * @param out the receiving quaternion
     * @param a quat to calculate W component of
     * @returns out
     */
    export function calculateW(out: GLMQuat, a: GLMQuat): GLMQuat;
}